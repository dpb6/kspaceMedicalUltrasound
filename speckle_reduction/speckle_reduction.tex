\chapter{Coherence and speckle reduction}
\section{Spatial and temporal coherence}
The concepts of spatial and temporal coherence are important in discussing the phase characteristics of imaging systems.  In general, the concept of coherence is related to the stability, or predictability, of phase.

Spatial coherence describes the correlation between signals at different points in space.  Temporal coherence describes the correlation or predictable relationship between signals observed at different moments in time.

Spatial coherence is described as a function of distance, and is often presented for zero-mean stationary signals as a function of correlation versus absolute distance between observation points $\rho_{AB}(|x_1-x_2|)$.  If $\big{\langle} P(x_1)P(x_2) \big{\rangle} = 0$, there is no spatial coherence at $|x_1-x_2|$, i.e.\ $\rho_{AB}(|x_1-x_2|) = 0$.

The same operation can be performed in time with the results plotted as correlation versus relative delay $\tau$. For the echo signal over the ensemble, temporal correlation is periodic at the inverse of the pulse repetition frequency (1/PRF), as shown in Figure \ref{fig:pri_corr}.  This holds for both transmit and receive, and for a point target or a speckle target.

\begin{figure}[h]
\centering
\includegraphicslarge{figures/pri_corr.eps}
\caption{(Top) Under multiple interrogations of a target with coherent radiation, the temporal correlation of the echo has periodicity within a pulse length and also at the pulse repetition interval. (Bottom) Incoherent radiation typically exhibits some finite correlation length, but only within the pulse length.}
\label{fig:pri_corr}
\end{figure}

Coherent radiation can be made to be incoherent by passing through a diffuser that randomizes phase over space, time, or both. E.g.:\ laser light passing through a spinning disk of frosted glass. 

The temporal correlation length of pulsed ultrasound is typically on the order of 1 $\mu$s, and in the case of repeated interrogation, is periodic.  Therefore the speckle target looks the same on each interrogation.  For incoherent radiation, there is no temporal coherence on the order of the pulse repetition interval.

All radiation has some finite correlation length. If you capture an image within the correlation length, you get a single speckle pattern. Laser light has a temporal correlation length on the order of seconds, therefore one can observe stable speckle patterns from scattering functions illuminated with laser light. Images captured at different times with incoherent radiation are uncorrelated, and are averaged in the final image if viewed with a detector whose response time is longer than the finite correlation length.

The spatial correlation of received echoes from a point target is constant.  For a speckle target insonified from a single position, the received echoes decorrelate with distance according to the van Cittert-Zernike curve (see the aperture domain chapter for more details). If this were not the case, spatial compounding would have no effect. Propagation is a low pass filter, therefore we expect the spatial correlation length to increase with distance from the source.

The spatial and temporal coherence of the radiation falling on a surface can be measured by changing the spacing between the two openings in the surface at $p_1$ and $p_2$, and observing the interference pattern that is generated on a screen beyond, as shown in Figure \ref{fig:young_int}. If the pattern dies out after the first fringe, the temporal coherence is on the order of one wavelength.  For laser light, the interference pattern is very wide.  If intensity patterns of \textbf{incoherent} radiation could be detected within its very short temporal correlation length, then the fringe pattern would extend out to infinity. Over many events, averaging flattens out this extended pattern.  Assuming some type of averaging is occurring, often in the detector, only waves with some finite coherence will interfere. In optics using an intensity detector, the number of averaged images approaches infinity.  The width of the interference pattern tells us about the temporal and spatial coherence of the radiation.

\begin{figure}[htbp]
\centering
\includegraphicsmed{figures/young_int.eps}
\caption{Schematic of Young's interference experiment, in which the intensity pattern cast on a screen by a dual-opening aperture is used to determine the temporal coherence length of the incident radiation. Only coherent radiation creates an interference pattern.  Given a mean incident intensity at the screen of $I_{0}$, perfect constructive interference produces an intensity of $4 I_{0}$.}
\label{fig:young_int}
\end{figure}

In optics, temporal coherence is also measured by combining beams from the same source but having a known path length difference, and observing the interference pattern produced. This path length difference is achieved using a Michelson interferometer, as shown in Figure \ref{fig:mich_int}.

\begin{figure}[htbp]
\centering
\includegraphicsmed{figures/mich_int.eps}
\caption{Schematic of a Michelson interferometer, in which the intensity pattern cast on a screen by interfering beams traveling along different path lengths can be used to determine the temporal coherence length of the incident radiation.}
\label{fig:mich_int}
\end{figure}
\clearpage
\section{The correlation coefficient in \textit{k}-space} 

The normalized correlation coefficient is the cross correlation function adjusted to remove effects related to the energy in the signals. This coefficient is often designated by the variable $\rho$. Given two random variables $x$ and $y$, the continuous time expression for $\rho_{x,y}$ is:

\begin{equation}
\rho_{x,y} = \frac{
\int^{\infty}_{-\infty}
(x-\mu_{x})
(y-\mu_{y})^{\ast}~dt}
{
 \sqrt{
  \int^{\infty}_{-\infty}(x-\mu_{x})^2~dt
  \int^{\infty}_{-\infty}(y-\mu_{y})^2~dt
 }
}
\end{equation}

As reviewed in Section \ref{random_review}, we can simplify this expression using that for variance. Here $\langle \rangle$ denotes ``expected value of'': 

\begin{table}[h]
\centering
\begin{tabular}{r r l}
The mean of $x$ &$\mu_{x}$ &$= \langle x \rangle$\\
The variance of $x$ & $\sigma_{x}^2$ &= $\big{\langle} (x-\mu_{x})^2 \big{\rangle}$\\
For $x$ zero-mean, &$\sigma_{x}^2$ &= $\langle x^2 \rangle$\\
The standard deviation of $x$ & $\sigma_{x}$\\
\\
The covariance of $x$ and $y$ & $\Bbb{C}_{x,y}$ &= $\big{\langle} (x-\mu_{x})(y-\mu_{y})^{\ast} \big{\rangle}$\\
The normalized correlation coefficient of $x$ and $y$ & $\rho$ &$= \frac{\Bbb{C}_{x,y}}{\sigma_{x}\sigma_{y}}$\\
&$\mu_{x+y}$ &$=\mu_{x} +\mu_{y}$\\
&$\sigma_{x+y}^2$&$=\sigma_{x}^2+\sigma_{y}^2 + 2\rho\sigma_{x}\sigma_{y}$\\
For $x,y$ independent & $\sigma_{x+y}^2$ &$=\sigma_{x}^2+\sigma_{y}^2$\\
For $x,y$ independent & $\langle x y \rangle$ &$=\langle x \rangle \langle y \rangle$ \\
$\therefore$ For $x,y$ independent, &$\rho_{x,y}$ &$= 0$
\end{tabular}
\end{table}

The correlation coefficient can be viewed as the dot product between two vectors and varies with the cosine of the angle between them. ``Half-way,'' the correlation has a value of 0.707 = $\sqrt{2}$.

% ************************************************** Insert with proof
%One can use these equations to calculate the effect of spatial compounding
%on the signal-to-noise ratio (SNR).  The compounding of two
%uncorrelated images produces an SNR improvement of $\sqrt{2}$.

The covariance definition of spatial and temporal coherence using Young's intereference experiment:
%(XXXX CHECK THIS)
\begin{equation}
\mathbf{C}_{P_{1},P_{2}}(P_{1},P_{2},\tau) = \big{\langle} \mathbf{u}(P_{1},t+\tau)
\mathbf{u}^{\ast}(P_{2},t) \big{\rangle}
\end{equation}
In Goodman this is referred to as the ``mutual coherence function''\cite{Goodman68}.

Given spatial stationarity:
\begin{equation*}
\mathbf{C}_{P_{1},P_{2}}(P_{1},P_{2},\tau) = \mathbf{C}_{P_{1},P_{2}}\big{(}|P_{1}-P_{2}|,\tau\big{)}
\end{equation*}

The covariance definition of temporal coherence: 
\begin{equation*}
\Gamma_{1,1}(\tau) = \mathbf{C}_{P_{1}}(P_{1},\tau) = \big{\langle} \mathbf{u}(P_{1},t+\tau)
\mathbf{u}^{\ast}(P_{1},t) \big{\rangle}
\end{equation*}
In Goodman this is referred to as the ``self coherence function''.

For the averaging of intensity patterns, as in compounding:
\begin{equation}
I_{1+2} = I_{1}+I_{2}+2\rho_{1,2}\sqrt{I_{1}I_{2}}
\end{equation}
Given $I_{1}=I_{2}$,
\begin{equation}
\begin{split}
\text{If }\rho=1&, I=4I_{1}\\
\text{If }\rho=-1&, I=0\\
\text{If }\rho=0&, I=2I_{1}\\
\end{split}
\end{equation}
Consider the vector notation $\overrightarrow{V}_{1}$ and $\overrightarrow{V}_{2}$ for the complex instantaneous amplitudes of two signals separated in phase by the angle $\phi$:
\begin{equation}
|\overrightarrow{V}_{1+2}|^2=|\overrightarrow{V}_{1}|^2+|\overrightarrow{V}_{2}|^2
 + 2|\overrightarrow{V}_{1}||\overrightarrow{V}_{2}|\cos\phi
\end{equation}

The \textit{power spectral density} of a signal $x(t)$ is:
\begin{equation*}
\begin{split}
\mathbf{G}(f) &= \text{lim}_{T\rightarrow\infty}\frac{|X(f)|^{2}}{T}\\
\text{where }X(f) &= \int^{\infty}_{-\infty}x(t)\exp(-j2\pi f t)~dt
\end{split}
\end{equation*}
In this terminology, Parseval's theorem is expressed:
\begin{equation*}
\int^{\infty}_{-\infty}x(t)x^{\ast}(t)~dt=\int^{\infty}_{-\infty}|X(f)|^2~df
\end{equation*}
in which phase information has been lost through conjugate multiplication.

The power spectral density of the output of a random process is the squared modulus of the transfer function of linear system times the power spectrum density of the input random process:
%(XXX CHECK THIS)
\begin{equation}
\Bbb{G}_{output}(f) = |H(f)|^2\Bbb{G}_{input}(f)
\end{equation}

The cross spectrum density is a measure of the spectral similarity of two signals at each complex frequency:
\begin{equation*}
\mathbf{G}_{u,v}(f) = \int^{\infty}_{-\infty}\Gamma_{u,v}(\tau)\exp(j2\pi f \tau)~d\tau
\end{equation*}
In terms of two complex signals $u(t)$ and $v(t)$ and their transforms $U(f)$ and $V(f)$,
\begin{equation*}
\mathbf{G}_{u,v}(f) =
\text{lim}_{T\rightarrow\infty}\frac{\big{\langle} U(f) V^{\ast}(f) \big{\rangle}}{T}
\end{equation*}
The limit as $T\rightarrow\infty$ is required to generalize this expression to include functions that do not have analytical Fourier transforms.

The cross correlation function and cross spectral density function are Fourier transform pairs. Neither contains meaningful phase information, and each contains equivalent information.
\begin{equation*}
\Gamma_{u,v}(\tau) = \int^{\infty}_{-\infty}
\mathbf{G}_{u,v}(f)\exp(-j2\pi f\tau)~df
\end{equation*}
For $u,v$ having equal phase profiles, $G_{uv}(f)$ is a purely real number due to the conjugate operation.  For unequal phase profiles, this quantity will be complex and exhibit interference patterns.

% ************* Problem here:  correlation coefficient is based on
% covariance, not correlation, hence the next equations are suspect:

%Integration of the \textit{k}-space overlap gives the correlation coefficient
%at $\tau=0$:
%\begin{equation*}
%\rho_{u,v}(0) = \frac{
%\int^{\infty}_{-\infty}\mathbf{G}_{u,v}(f)~df}
%{
% \sqrt{
%  \int^{\infty}_{-\infty}|U(f)|^2~df
%  \int^{\infty}_{-\infty}|V(f)|^2~df
% }
%}
%\end{equation*}
% NEED EQUATION HERE

If we know the \textit{k}-space windows of a system and target function, or two different windows of the system under compounding, the product of these windows gives the correlation between their echo signals.

Envelope detection shifts the signal band to base band, losing the carrier frequency.  Ideally, everything but the carrier frequency is preserved, e.g.\ axial and lateral bandwidth are preserved.  The $\rho_{u,v}(0)$ of the detected signals is the square of $\rho_{u,v}(0)$ of the corresponding RF signals.

By translating the \textit{k}-space window developed by the imaging system, more information can be gathered from the target spatial frequency space. The averaging of the detected speckle patterns from such shifted windows in \textit{k}-space is a means of reducing speckle noise, and is achieved through \textbf{spatial} or \textbf{frequency compounding}.  The correlation coefficient among the \textit{k}-space windows will tell us if compounding will reduce the speckle. (If the original spectra are in phase, the correlation coefficient is proportional to overlap. This is the case under ideal conditions, i.e.\ at the focus and with no aberration.)

\section{Spatial compounding}
The transducer-target geometry affects the spatial frequencies in the target function that are interrogated by the imaging system. The effect of movement of either relative to the other is shown in Figure \ref{fig:sing_arc}.  Spatial compounding is achieved by the translation of the aperture, as shown in Figure \ref{fig:spa_com}. The detected signals acquired at different aperture positions are averaged together.

\begin{figure}[h]
\centering 
\includegraphicssmall{figures/sing_arc.eps}
\caption{If you move a single element with respect to a fixed group of scatterers, you sweep out an arc in \textit{k}-space.}
\label{fig:sing_arc}
\end{figure}

\begin{figure}[h]
\centering 
\includegraphicsmed{figures/spa_com.eps}
\caption{Under spatial compounding, the aperture is translated laterally (left) between interrogations of the same region of interest.  This shifts the system's region of support in \textit{k}-space, reflecting decorrelation of the response, and hence of the observed speckle patterns. The region of overlap (shaded) is proportional to the correlation between the two speckle patterns.}
\label{fig:spa_com}
\end{figure}

\begin{figure}[h]
\centering 
\includegraphicsmed{figures/aper_sep.eps}
\caption{As the sub-apertures of a 2:1 spatial compounding system are separated in space, the normalized correlation of the signals received by them changes as a function of this separation.}
\label{fig:aper_sep}
\end{figure}

In a classic paper on speckle second-order statistics by Wagner, \textit{et al.}, we read that the lateral spatial correlation of speckle is equal to the autocorrelation of a triangle function\cite{Wagner88}.  Translation of one aperture length takes the cross correlation curve to 0, and hence produces statistically independent speckle patterns, but even translation of only 1/4 aperture length reduces the cross correlation function to a very low value, as shown in Figure \ref{fig:aper_sep}. The correlation curve of the complex signal $\rho_A(x)$ is symmetric as a function of lateral displacement $\Delta x$ and is given in piecewise form by O'Donnell, normalized by aperture length\cite{ODonnell88}. The correlation function used for compounding the magnitude signal, $\rho_M$, is given by the square of the complex amplitude correlation:

\begin{equation}
\rho_A(\Delta x)=\left\{
\begin{array}{lr}
1-6\Delta x^2+6\Delta x^3 & \Delta x\leq 1/2\\
2(1-\Delta x)^3 & 1\geq \Delta x > 1/2\\
0 & \Delta x \geq 1
\end{array}\right.
\end{equation}
\begin{equation}
\rho_M=\rho_A^2
\end{equation}

%\textit{Here, present analysis of spatial compounding in terms of lesion contrast detail, showing that compounding improves lesion detectability.\\ $SNR_{lesion} = \sqrt{\frac{3.2}{2}}$\\The drawback is that doctors do not like averaged images, which appeared to be ``washed out''.}
%\clearpage

With a given acoustic window, the correlation curve can be used to optimize the number of subapertures used to form the compounded image\cite{Trahey86a}. The variance of the compounded image from $n$ images $X_k$, given the individual variances $\sigma^2(X_k)$ and the correlation $\rho(X_k,X_j)$ is:

\begin{equation}
\sigma^2\left(\sum_{k=1}^n\frac{X_k}{n}\right)=\sum_{k=1}^n\frac{\sigma^2(X_k)}{n^2}+2\sum_{k=1}^n\sum_{j=k+1}^n\frac{\rho_M(X_k,X_j)\sigma(X_k)\sigma(X_j)}{n^2}
\label{eqn:compounding}
\end{equation}

\section{Frequency compounding}
Frequency compounding adds together detected data from different frequency bands.  These frequency bands have different regions of support in \textit{k}-space, as shown in Figure \ref{fig:freq_com}.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/freq_com.eps}
\caption{Under frequency compounding, the temporal frequency band used in imaging is altered among different interrogations of the same region of interest (left).  This shifts the system's region of support along the $k_{z}$ dimension in \textit{k}-space, reflecting decorrelation of the response, and hence of the observed speckle patterns. The region of overlap is proportional to the correlation between the two speckle patterns.}
\label{fig:freq_com}
\end{figure}

A simple frequency compounding method is to transmit a single broad bandwidth pulse and to then select different receive frequency ``sub-bands'' through the application of filters, as shown in Figure \ref{fig:fcomp_bands}.  The axial resolution is now determined by the smaller bandwidth of these compounding filters.  This represents a trade-off between speckle SNR and axial resolution.

Again, the correlation between sub-bands is a function of their overlap.  Completely discrete bands have no correlation.  The signal-to-noise ratio will be different in each band, thus ``pre-whitening'' filters may be used to level off each band, although doing so amplifies portions of the noise spectrum. The complex signal correlation $\rho_A$ is determined by the frequency separation $\Delta f$ and the bandwidth $BW$:

\begin{equation}
\rho_A(\Delta f)=e^{-1/2(\Delta f/BW)^2}
\label{eqn:freqcompounding}
\end{equation}

\begin{figure}[h]
\centering
\includegraphicssmall{figures/fcomp_bands.eps}
\caption{Under frequency compounding, a broad temporal frequency band is used on transmit.  The receive signal is filtered to produce several receive sub-bands (3, in the diagram above.) Frequency compounding relies on detection of these sub-bands before summation, which averages the partially-uncorrelated speckle patterns they produce. The region of sub-band overlap is proportional to the correlation between the speckle patterns.}
\label{fig:fcomp_bands}
\end{figure}

Each sub-band typically has a Gaussian profile axially.  Therefore as we change center frequencies of the compounding bands and calculate their cross correlation, we are calculating the autocorrelation function of a Gaussian. For a frequency compounding system, the correlation coefficient between sub-band speckle patterns as a function of the separation of sub-bands is thus the autocorrelation function of a Gaussian.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/gaus_sep.eps}
\caption{As the bands of two receive filters are separated (top), the normalized correlation of the signals received by them changes as a function of the separation in center frequency (bottom).}
\label{fig:gaus_sep}
\end{figure}

Note that frequency compounding only works on detected data. Averaging RF signals is simply superposition, which recreates the original bandwidth system but achieves no compounding.

Trahey and Smith investigated the speckle reduction vs.\ spatial resolution trade-off using an expression for lesion detectability based on contrast-detail study. As in spatial compounding, equations \ref{eqn:compounding} and \ref{eqn:freqcompounding} can be used to predict image quality as a function of compounded images. For a transducer capable of exciting its whole bandwidth in a single pulse, this work suggested that it is better to use the entire bandwidth coherently rather than subdivide it for compounding\cite{Trahey86}. 

On the other hand, there is reason to believe commercial scanners are routinely using frequency compounding.  This reflects the fact that the axial resolution of a typical scanner's display monitor is not capable of exhibiting the true axial resolution of the system. Given this ``excess'' of axial resolution, frequency compounding can be applied without an apparent penalty in axial resolution. This cannot be done when the scanner is operated in a ``zoom'' mode.

%\clearpage
