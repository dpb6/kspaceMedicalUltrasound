\chapter{Aperture domain analysis}

\section{The van Cittert-Zernike theorem}
%There are two ways to reduce the correlation coefficient between speckle patterns:
%1) reduce to the \textit{k}-space overlap (compounding) of the  acquisition systems
%2) acquire the patterns with different phase profiles (aberration,quantization error, focal error)

The echoes from a speckle-generating target have a finite spatial correlation length due to aperture-scatterer path length differences that distinguish one receive channel position from another. In contrast, the echoes from a point target have infinite spatial correlation length due to the lack of such path length differences. 

Although the transmitted pulse in ultrasound is coherent, speckle-generating diffuse media serves as a truly incoherent source -- the location of scatterers on one line is uncorrelated with that of those in the next -- and produces incoherent backscattered echoes. The van Cittert-Zernike theorem states that for an incoherent, diffuse source, the spatial covariance function $R$ is proportional to the Fourier transform of the intensity pattern of the incoherent source\cite{Mallart91}:

\begin{equation}
R(x_1-x_2,z,f)\propto\iint_S|H(x,z,f)|^2e^{-j2\pi f\frac{x\cdot(x_1-x_2)}{zc}}d^2x.
\end{equation}

The incoherent source is modeled by the multiplication of the transmit pressure field, the Fourier transform of the transmit aperture function (including apodization and focusing) $H(x,z,f)$, and the random sub-wavelength scatterers $\chi(x,z,f)$.  This result omits a scaling term and a phase term present when $x_1$ and $x_2$ are not symmetric about zero. It is stationary with respect to channel positions $x_1$ and $x_2$, only depending on the spacing, or lag, between them. Note that the extent and shape of the coherence curve is defined only by the transmit aperture, while the measurement is described in terms of receive aperture spatial lag. This equation can equivalently be written as the autocorrelation of the transmit aperture function.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/tx_rx_sep.eps}
\caption{The cross-correlation between speckle signals from the focus received on two elements of an array obeys the predictions of the van Cittert-Zernike theorem (solid line).  Here this curve is compared to the cross-correlation between apertures given lateral translation on receive only (dashed) and on transmit and receive (dotted).}
\label{fig:tx_rx_sep}
\end{figure}

Experimentally, coherence can be measured at every spatial location using the covariance of the received channel echo signals after focusing $s_i$ as a function of lag $m$:

\begin{equation}
\hat{C}(m)=\frac{1}{N-m}\sum_{i=1}^{N-m}\sum_{n=n_1}^{n_2}s_i(n)s_{i+m}(n).
\end{equation}

The estimated covariance is impacted by the length of the kernel, from $n_1$ to $n_2$. While shorter kernels underestimate the average coherence across all lag values, longer kernels sacrifice axial resolution. It is even possible to make a coherence estimate using a single sample with the instantaneous phase of the complex signal\cite{Trahey2014}. In practice, it may be advantageous to use the normalized cross-correlation in place of the covariance to account for signals with different amplitudes.

The above equations are not necessarily restricted to the spatial location of the transmit focus. The coherence of a point away from the fixed transmit focus can calculated using the complex transmit aperture with an additional phase term. The phase of the transmitted wave is made up two components -- the transmit focal delays and the accumulated phase due to propagation from each array channel -- that cancel completely at the focal point. The residual phase term present when these two do not completely cancel incoherently sums across the channels and reduces coherence away from the focal point\cite{Bottenus2013a}. Synthetic aperture imaging methods produce a transmit focus without these residual terms through depth and extend the depth of field for the signal coherence. 

The principle of acoustic reciprocity also implies that it is possible to measure coherence from multiple transmit locations corresponding to the summed signal across the receiving aperture, reversing the roles of transmit and receive\cite{Bottenus2015c}. Due to the assumed linearity of the propagation and scattering processes, the situation is completely analogous to the above and the predicted coherence is given by the complex autocorrelation of the receive aperture function. In practice, this coherence can be measured using synthetic aperture signals that are stored as a function of transmit event, where the receive aperture signals for each have been focused and summed.

%The temporal coherence length of pulse ultrasound is infinite since one pulse will correlate with later pulses.  However, it's spatial coherence length is determined by the aperture positions.  This is why synthetic receive sub-apertures should be inter-laced since the signals from independent sub-apertures would be poorly correlated. This would prevent tissue motion compensation based on sub-aperture signal alignment.

\section{Phase aberration}
% Beginning of PhD dissertation excerpt, from
% /home/mea1/tex/phd/back.tex and 
% /home/mea1/tex/phd/aber.tex with associated figures
\begin{figure}[htbp]
\centering
\includegraphicssmall{figures/aber1_curve.eps}\hspace{0.75 in}\includegraphicssmall{figures/aber2_curve.eps}
\caption{(left) The first order phase aberration corresponding to a 5\% overestimation in sound speed at the f/1 focal range. (right) One realization of a simulated 50 ns RMS second order aberrator with a spatial autocovariance FWHM of 3 mm. This particular aberrator creates 41.5 ns RMS phase error across the array, and has a spatial autocovariance FWHM of 2.7 mm.}
\label{fig:aber1_curve}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphicssmall{figures/aber1_psf.eps}\hspace{0.75 in}\includegraphicssmall{figures/aber2_psf.eps}
\caption{(left) The impact of the first order aberrator (sound speed error) shown in Fig.~\ref{fig:aber1_curve} on the system point spread function.  The aberrated case (bottom) is compared to the unaberrated control (top).  The ``sharpness'' of the PSF is markedly reduced, reflecting losses of spatial and contrast resolution.  The images are not displayed with the same gray-scale, so they do not show the 75\% reduction in PSF amplitude that the aberration causes. (right) The impact of the second order aberrator shown in Fig.~\ref{fig:aber1_curve} on the system point spread function. As in the case of first order aberration, the ``sharpness'' of the PSF is reduced, reflecting losses of spatial and contrast resolution. Once again, the images are not displayed with the same gray-scale, so they do not show the 91\% reduction in PSF amplitude caused by the aberration.} 
\label{fig:aber1_psf}
\end{figure}

The spatial and contrast resolution of medical ultrasound can be severely limited by a phenomenon known as phase aberration.  The steering and focusing of an ultrasound beam using a phased array relies on an approximation of the speed of sound in tissue, usually 1540 m/s.  In fact, the speed of sound through different tissues can vary greatly.  As an acoustic wavefront passes through inhomogeneous tissues, it can become distorted as portions of its surface are advanced or retarded.  On transmit this phenomenon affects the focusing and steering of the system point spread function (PSF). The returning echoes incident on the elements of the transducer array are also misaligned such that when these signals are summed to form a single echo line they no longer sum coherently, resulting in focal and steering errors, and consequently, diminished echo amplitude. 

The discussion is separated into two separate sections, on ``first'' and ``second'' order aberrators.  In this terminology, first order refers to geometric focal errors due to the difference between the actual mean sound speed in the tissue and that assumed by the ultrasound scanner beamformer. Second order aberration refers to the effects of the inhomogeneity of the tissue that still remain after the first order focal error is removed.  Both types of aberration result in blurring of the point spread function and consequently the reduction of spatial and contrast resolution.

A first order aberrator arises from a gross sound speed error in the ultrasound system beamformer relative to the actual tissue, and is usually caused by the application of a single assumed tissue sound speed to all patients.  For example, the first order aberration resulting from a 5\% overestimation of sound speed is shown in Fig.~\ref{fig:aber1_curve}. The impact of this aberrator on the system point spread function is shown in Fig.~\ref{fig:aber1_psf}. In this example, the aberrator was applied to the element delays of a typical array at the f/1 focal range (7.5 MHz, 60\% relative bandwidth, 128 elements of width equal to the 1540 m/s wavelength, f/8 in elevation) in an acoustic field simulation program\cite{Jensen92}.  The aberration leads to an 85\% reduction in the PSF energy and a 75\% reduction in its peak amplitude.  The lateral resolution has been degraded by a factor of 3.9. 

An example of a second order aberrator is shown in Fig.~\ref{fig:aber1_curve}. The impact of this aberrator on the system point spread function is shown in in Fig.~\ref{fig:aber1_psf}.  Once again, this aberrator was applied to the element delays of a typical array (7.5 MHz, 60\% relative bandwidth, 128 elements of width equal to the wavelength, f/8 in elevation) in an acoustic field simulation program\cite{Jensen92}.  The aberration produces an 95\% reduction in the PSF energy and a 91\% reduction in its peak amplitude.  The lateral resolution in this case has been degraded by a factor of 2.3.

Cross-correlation methods\cite{Flax88} are affected by VCZ.  The VCZ curve falls off even faster in the presence of a transmit phase aberrator due to be broadening of the beam, which scales in \textit{k}-space to a narrower intensity pattern, thereby producing a steeper correlation curve.

A variety of techniques have been proposed to compensate for the effects of phase aberration\cite{Moshfeghi88,ODonnell88a,Sumino91,Trahey91a,Zhu92,Freiburger92,Zhu94,Hinkelman94,Hinkelman95,Zhu95,Anderson98b}. Ideally, the estimated aberrator should be applied on both transmit and receive, as the phase screen affects wave propagation on both transmit and receive.

% End of PhD dissertation excerpt.

\section{Reverberation and off-axis scattering}
Acoustic wave propagation is commonly discussed in terms of the first Born approximation -- that the wave field can be described in terms of incident (transmitted) wave and the backscattered waves that are used to produce the ultrasound image. This model does not account for multiply scattered waves that reflect many times before returning to the transducer, violating the assumed propagation path and timing used in receive focusing. This reverberation often produced in near-field \textit{in vivo} layers is written into the image at all depths as clutter. The effect is especially pronounced in hypoechoic targets where the echo magnitude from the target is low, allowing the clutter signal to dominate the resulting image. Simulations have shown that even with perfect aberration correction, the impact of reverberation clutter is significant in fundamental imaging\cite{Pinton2011}.

Similarly, off-axis scatter produces clutter that obscures targets in the final ultrasound image. Echoes are produced by the side lobes of the point spread function, which are worsened by effects such as aberration, that return at the same time as the echoes from the target. Even though the side lobes have a lower amplitude than the main lobe, the echoes produced from even background tissue can easily overwrite a low-amplitude signal within a lesion or vessel.

These effects can be viewed in the aperture domain, looking at the echo signals across the receiving array\cite{Byram2014} as shown in Fig.\ref{fig:apertureclutter}. A single scatterer at the focus of the transducer appears across the receiving array as a constant value, the signal is incident on all channels simultaneously (after focusing). A scatterer located off-axis laterally produces a sinusoidal signal, multiple cycles of the reflected wave reaching the aperture at regular spatial intervals based on the wavelength. An echo that undergoes multiple scattering before reflection from a different depth, arriving at the transducer coincidentally with signals from the target depth, produces a chirp signal, or a sinusoid with linearly varying frequency.\\\\

\begin{figure}[h]
\centering
\includegraphicslarge{figures/clutter}
\caption{Aperture-domain model for clutter after fixed receive focusing delays are applied. (a) A scatterer at the focus produces a wavefront that is flat -- coherent across the aperture. (b) A lateral off-axis scatterer produces a sinusoid with a frequency dependent on distance. (c) An axial off-axis scatterer that is insonified after multiple scattering produces a chirp signal with linearly varying frequency.}
\label{fig:apertureclutter}
\end{figure}

The sum of all the reverberation clutter from within the scattering layers appears as incoherent noise in the aperture domain\cite{Pinton2014}. The coherence from the sum of these signals is approximately a delta function, showing little correlation between channels on the array.  This behavior is distinct from the effect of phase aberration, which acts as a spatially-correlated phase shift of the receive signals. This signal is independent from the backscattered field and the resulting coherence curve is approximately the sum of the delta function and the Fourier transform of the transmitted field intensity, each scaled by their relative magnitudes. In the absence of other clutter effects, the magnitude of the delta function relative to the coherence curve of tissue (the triangle function) indicates the effective signal-to-noise ratio due to the reverberation for an array of length $N$\cite{Bottenus2013a}:

\begin{equation}
\rho(n)=\frac{1-\frac{n}{N}}{1+\mbox{SNR}^{-1}}
\end{equation}

This equation also applies to additive noise, which generates incoherent clutter in both space and time\cite{Bottenus2015}. Thermal noise produced in the receiving electronics, unlike reverberation, increases with depth relative to the backscattered echoes due to attenuation and geometric spreading of the ultrasound waves. Reverberation clutter and additive noise both degrade image quality and preferentially obscure hypoechoic targets by effectively adding a random component to the receive apodization of the array. With the apodization model, these two effects can be modeled using the conventional VCZ theorem framework.

\begin{figure}[h]
\centering
\includegraphicslarge{figures/additiveNoise}
\caption{Autocorrelation of an aperture with an additive random component, modeling incoherent additive noise or reverberation. The magnitude of the delta function relative to the triangle function indicates the signal-to-noise ratio of the backscattered echoes.}
\label{fig:additiveNoise}
\end{figure}

\section{Coherence imaging}
Coherence imaging uses the measured coherence of the backscattered echoes to suppress superimposed clutter -- additive noise, off-axis scattering, aberration and reverberation. These signals are differentiated from diffuse tissue signals in the aperture domain as described above. Two example techniques are presented here that represent identifying clutter in two different domains.

\subsection{Generalized coherence factor}
The generalized coherence factor (GCF) is a weighting applied to each pixel of the B-mode image based on its aperture-domain frequency properties\cite{Li2003a}. The focused echoes from all channels, demodulated to baseband, are used to compute the Fourier spectrum across the aperture $p(k)$ for each spatial location using the discrete Fourier transform. The weighting is then calculated as the sum over a low-frequency region normalized by the sum over all frequencies, or the ratio of coherent energy to total energy:

\begin{equation}
GCF=\frac{\sum_{k=0}^M|p(k)|^2}{\sum_{k=0}^{N-1}|p(k)|^2}
\end{equation}

\begin{equation}
x_{corrected}=x\cdot GCF
\end{equation}

As described above, a target at the focus appears across the aperture as a constant, or a delta function in frequency space located at DC. Speckle-generating media is only partially coherent, so it is expected be represented at low frequencies somewhat beyond DC. Off-axis scatter appears as a sinusoid in the aperture domain, or a delta function shifted to higher frequencies in frequency space. Reverberation appears as a chirp signal, spanning a range of frequencies. By weighting pixels that contain more low-frequency energy more heavily, speckle and coherent targets are preserved while clutter-dominated signals are suppressed.

\subsection{Short-lag spatial coherence imaging}
Short-lag spatial coherence (SLSC) imaging creates an image solely from the measured spatial coherence information, rather than augmenting the B-mode image. The focused echoes from all channels are first used to calculate the normalized cross-correlation curve as a function of lag at every spatial location:

\begin{equation}
\rho[z,x,n]=\\\frac{1}{N-n}\sum\limits_{i=1}^{N-n}{\frac{\sum\limits_{k=k_1}^{k_2}{s[k,x,i]s[k,x,i+n]}}{\sqrt{\sum\limits_{k=k_1}^{k_2}{s^2[k,x,i]}\sum\limits_{k=k_1}^{k_2}{s^2[k,x,i+n]}}}}.
\end{equation}

Axial kernels on the order of a wavelength from sample $k_1$ to $k_2$ are used in the cross-correlation and are centered on the sample at depth $z$. Each pixel $R_{sl}[z,x]$ in the SLSC image is created by summing the corresponding coherence curve up to $M$ lags and normalization is applied to the resulting image\cite{Dahl2011a}:

\begin{equation}
R_{sl}[z,x]\propto\sum\limits_{n=1}^M\rho[z,x,n].
\label{eqn:slscsum}
\end{equation}

The premise of this method is that the clutter signals described above are best differentiated from the tissue signal in the short-lag region. Incoherent clutter due to reverberation and noise is best suppressed because it is approximately zero everywhere except at a lag of zero, while partially coherent aberration and off-axis scattering clutter is more similar to the tissue signal but still distinguishable due to reduced coherence in the short-lag region. 

The area between the coherence curves for background and target is the source of contrast for this technique, so even if a uniform clutter is present that degrades both, contrast is largely preserved. In fact, the presence of noise and clutter is the differentiating factor between background tissue and weakly scattering, but still coherent, hypoechoic tissue. This relative decorrelation rate and the amplitude scattering function itself are the two factors that determine the contrast of the SLSC image.