function [] = makeClutterFig()
clf
set(gcf,'Position',[25 51 875 400]);
set(gcf,'PaperPositionMode','auto')

%Aperture setup
ap_x=linspace(-10,10,500);
ap=[ap_x' zeros(length(ap_x),1)];
focus=[0 20];

%Waveform setup
t=linspace(-10,10,1000);
f0=1;
wave=sin(2*pi*f0*t);

%Plotting setup
color=[0 0 0]; % Wavefront arrival line color
lw=3;            % Line width for wavefront arrival line and aperture
gr=ones(1,3)*.7; % Gray value for scattering point
mkw=1;           % Border size for focal point
mksz=10;         % Scatterer marker size
N=4;             % Number of wavefronts to draw
scl=2;           % Draw every nth wavefront
fsz=14;          % Text font size

%Plot constant wavefront
pt=focus;
subplot(331); drawWaveArrival();
subplot(3,3,[4 7]); drawField();
text(-9,focus(2)+5,'(a) At the focus','FontSize',fsz)

%Plot off-axis scatter
pt=[7,20];
subplot(332); drawWaveArrival();
subplot(3,3,[5 8]); drawField();
text(-7,focus(2)+5,'(b) Off-axis','FontSize',fsz)

%Plot multi-path
pt=[0,10];
subplot(333); drawWaveArrival();
subplot(3,3,[6 9]); drawField();
text(-8,focus(2)+5,'(c) Multi-path','FontSize',fsz)

function t_resid = drawWaveArrival()

t_arrive=sqrt(sum((ap-repmat(pt,size(ap,1),1)).^2,2));
t_delay=sqrt(sum((ap-repmat(focus,size(ap,1),1)).^2,2));

t_resid=interp1(t,wave,t_arrive-t_delay);

plot(ap_x,t_resid,'LineWidth',lw,'Color',color)
axis([ap_x(1) ap_x(end) -2 2]);axis off
end

function [] = drawField()
plot(pt(1),pt(2),'o','MarkerEdgeColor',gr,'MarkerFaceColor',gr,'MarkerSize',mksz);hold all
plot(focus(1),focus(2),'ko','LineWidth',mkw,'MarkerSize',mksz);
plot([ap_x(1) ap_x(1) ap_x(end) ap_x(end)],[-2 0 0 -2],'k-','LineWidth',lw);
for i=1:N
    r=sqrt((1+i*scl*f0).^2-(ap_x*2-pt(1)).^2);r(real(r)~=r)=NaN;
    plot(ap_x*2,pt(2)-r,'--','Color','k');
end
axis ij;axis image;axis([ap_x(1)-1 ap_x(end)+1 -2 focus(2)]);axis off
end
end