\chapter{Ultrasound and \textit{k}-space}
\section{\textit{K}-space transforms of elementary geometries}
Consider the simple geometry of two sound point sources and an observation plane, shown in Figure \ref{fig:source_pair}. This corresponds to an impulse pair aperture function, and the FA predicts that we will observe a sinusoidal far-field pattern. This pattern can also be constructed by geometrically predicting the locations of constructive and destructive interference between waves from the two sources.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/source_pair.eps}
\caption{Consider the field produced by a pair of sound sources generating constant 1 MHz signals in phase. If we moved a hydrophone across the field it would measure a 1 MHz signal modulated in amplitude and shifted in phase. Locations of constructive interference, such as \textit{A} and \textit{C}, alternate with locations of destructive interference, such as \textit{B}.}
\label{fig:source_pair}
\end{figure}

The Fourier transform of the aperture gives the lateral modulation of the point spread function.  The Fourier transform of this point spread function gives the transmit-only or receive-only transfer function of the system in \textit{k}-space.  The system response is equal to the product of the receive-only and the transmit-only responses:

\begin{equation}
TxRx(\theta) = Rx(\theta) \times Tx(\theta)
\end{equation}

We refer to the transmit-receive response $TxRx(\theta)$ as the \textbf{pulse-echo} response.  For the two-source geometry shown in Figure \ref{fig:source_pair}, the pulse-echo response is contrasted with the transmit-only response in Figure \ref{fig:source_square}. The corresponding \textit{k}-space representations are shown in Figure \ref{fig:impulsepair_squared}.

\begin{figure}[h]
\centering
\includegraphicslarge{figures/source_square.eps}
\caption{The transmit-only (left) and transmit-receive (right) amplitude in the observation plane of a two-element system.}
\label{fig:source_square}
\end{figure}

\begin{figure}[h]
\centering
\includegraphicslarge{figures/impulsepair_squared.eps}
\caption{The transmit-only (left) and transmit-receive (right) lateral \textit{k}-space responses of a two-element system.}
\label{fig:impulsepair_squared}
\end{figure}

These solutions for a simple two-source geometry have been for the monochromatic case, i.e.\ for constant insonification with a single frequency.  In these cases the far-field interference pattern in the observation plane technically stretches to infinity.  If we consider a broadband case in which each source launches a short pulse, interference occurs only over a limited region in the observation plane, as shown in Figure \ref{fig:interference}.  Outside of this region, the path length difference between the two sources and the observation point exceeds the pulse length, and hence interference no longer takes place.

\begin{figure}[h]
\centering
\includegraphicslarge{figures/interference.eps}
\caption{Hydrophone responses to pulses emitted by single-source (top) and double-source (bottom) systems. Neglecting diffractive effects, in the single-source system the pulse shape is relatively constant across the field. In the double-source system, interference occurs at points in the field where the pulses overlap. Beamforming is in essence the manipulation of this interference.}
\label{fig:interference}
\end{figure}

As shown in Figure \ref{fig:two_el}, a two-element system with two-frequency output is sensitive to six spatial frequencies, with the redundant frequencies having twice the amplitude of the outliers.  As the target gets further away, the outlying spatial frequencies move in as the point spread function spreads out.

\begin{figure}[tbp]
\centering
\includegraphicslarge{figures/two_el.eps}
\caption{(Top) A two-element system transmitting and receiving on two temporal frequencies (left) has a \textit{k}-space response with six distinct spatial frequency components (right).  (Bottom) These components are also shown separated into five groups in \textit{k}-space.}
\label{fig:two_el}
\end{figure}

\section{Spatial resolution and spatial frequencies}
The relationship between spatial resolution and spatial frequency response can be illustrated with an example based on a rudimentary imaging system.  Consider the imaging of two point targets with a single point transducer, as shown in Figure \ref{fig:point_pair}. This transducer has no lateral resolution whatsoever, and is capable only of resolving targets in range.  Another way of stating this is that this imaging system is not sensitive to lateral spatial frequencies. When the point target are equidistant, i.e.\ only separated in the lateral dimension, it is clear that their echoes cannot be resolved by the point transducer. When the transducer is moved through an arc, the lateral and axial spatial frequencies of the original transducer/target geometry are gradually exchanged. When the transducer has been rotated 90$^\circ$ about the targets, the targets can be resolved provided they are separated by more than the axial resolution of the transducer (i.e.\ approximately half the pulse length). It is instructive to consider this example in \textit{k}-space using the rotation property of the 2-D Fourier transform. 

\begin{figure}[h]
\centering
\includegraphicslarge{figures/point_pair.eps}
\caption{A simple point transducer imaging system has axial but no lateral resolution.  When two point targets are separated in the lateral dimension only, (left) this system cannot resolve them.  By rotating the system through an arc relative to the targets, (center, right) the axial and lateral spatial frequencies of the original transducer/target geometry are exchanged, such that the final orientation (right) allows the targets to be resolved.}
\label{fig:point_pair}
\end{figure}

By extension, this example demonstrates why an aperture with some lateral extent is required to provide lateral resolution (i.e.\ lateral extent provides sensitivity to lateral spatial frequencies), and that given a finite target range and a very short pulse, the lateral resolution will only equal the axial resolution if the aperture becomes infinitely large.

\section{\textit{K}-space transforms of large apertures and arrays}
The idealized \textit{k}-space examples presented so far consider monochromatic imaging systems with point sources and receivers. Practical imaging systems utilize elements of some finite lateral extent in the aperture, producing pulses with some bandwidth. The \textit{k}-space response of such a system has some extent, i.e.\ it forms a ``\textit{cloud}'' rather than being composed of delta functions. As the number of array elements is increased, this response covers a larger region of support, as seen in Figure \ref{fig:four_el}. A sparse array creates gaps in the \textit{k}-space response.  The lateral response and can be predicted by the convolution of the transmit and receive aperture functions, as shown in Figure \ref{fig:four_elconv}.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/four_el.eps}
\caption{A four-element system (left) and its \textit{k}-space response (right).}
\label{fig:four_el}
\end{figure}

\begin{figure}[h]
\centering
\includegraphicsmed{figures/four_elconv.eps}
\caption{The lateral \textit{k}-space response of the four-element system (right) can be approximated by the auto-convolution of its aperture function (left).}
\label{fig:four_elconv}
\end{figure}

With a large aperture and negligible kerf, the \textit{k}-space magnitude response becomes smooth and continuous over a finite region. For a rectangular aperture producing a pulse with a Gaussian frequency response, this \textit{k}-space frequency response is shaped like an asymmetrical mountain, with a tear-drop shaped footprint.

\begin{figure}[h]
\centering
\includegraphicsxsmall{figures/teardrop.eps}
\caption{In the lateral dimension, the cross-section of this function is a triangle. In the axial dimension, the cross-section is Gaussian.}
\label{fig:teardrop}
\end{figure}

The lateral pulse-echo beam pattern for a rectangular aperture is a sinc$^2$ function.  The lateral \textit{k}-space response is therefore a triangle function.  For a broadband transducer, the axial response is often approximated as a Gaussian centered at the transducer's center frequency. These responses are shown in Figure \ref{fig:lat_ax}.  The lateral pulse-echo beam pattern and \textit{k}-space response is for this aperture are seen to be a product in space and a convolution in frequency of the transmit-only cases, respectively, as shown in Figure \ref{fig:rect_aper}.
%These responses change throughout the isochronous volume.
The axial response is an acoustic pulse that becomes progressively stretched off axis, with destructive interference generally increasing progressively off axis.

\begin{figure}[h]
\centering
\includegraphicsfull{figures/lat_ax.eps}
\caption{An ultrasound system has responses in both lateral (left) and axial (right) spatial frequencies.  The axial spatial frequencies are usually presented as temporal frequencies. A typical system has a pass-band axial response centered at the transducer's center frequency, $f_{c}$.}
\label{fig:lat_ax}
\end{figure}

\begin{figure}[h]
\centering
\includegraphicslarge{figures/rect_aper.eps}
\caption{(Top) The transmit-only (left) and transmit-receive (right) amplitude in the observation plane of a rectangular aperture. (Bottom) The transmit-only (left) and transmit-receive (right) lateral \textit{k}-space responses of a rectangular aperture.}
\label{fig:rect_aper}
\end{figure}

The triangular lateral response is due to the presence of a larger number of elements at close spacing, while a smaller number of elements are placed at large spacing.  Hence the weighting of the frequency response reflects the weighting of the sampling of spatial frequencies in the aperture, and is directly related to spatial resolution, as shown in Figure \ref{fig:lat_res}.  The elements at the ends of the array are creating the highest spatial-frequency signals. If you remove the elements from the ends, the spatial frequency response gets narrower.  The limited spatial resolution that results is not a sampling effect, but the result of the filtering effect of propagation from a limited aperture.  Hence this limit is referred to as the \textbf{diffraction-limited resolution} of the system. In a typical system with an f-number on the order of 2, the lateral to axial spatial frequency mismatch can be on the order of 10!

\begin{figure}[h]
\centering
\includegraphicsmed{figures/lat_res.eps}
\caption{The extent of the region of support of the lateral \textit{k}-space representation of an imaging system defines its lateral spatial resolution.}
\label{fig:lat_res}
\end{figure}

%\textit{DIAGRAM here of aperture, point spread function, and lateral \textit{k}-space
%response, and maximum amplitude}
%Here we introduce the concept of the isochronous volume, which is a
%snapshot over space of the point spread function of the system at the
%focus.
%Note that the maximum-amplitude beam plot does not account for energy
%or synchrony. It is better to use the integral of energy over time, or
%the r.m.s. amplitude to account for these effects.
%\clearpage

\section{{K}-space is a two-dimensional frequency space}

General principles of \textit{k}-space analysis:
\begin{itemize}
\item   Think about the transfer function in two dimensions.
\item	New surface area is equivalent to new information.
\item	An ideal imaging system is an all-pass filter.
\item   A realistic system has an impulse response, or point spread function, the Fourier transform of which is the spatial frequency response in \textit{k}-space.
\item   Improvements in the point spread function correspond to a greater region of support in \textit{k}-space, reflecting an improved capability to gather more information.
\end{itemize}

For example, if the \textit{k}-space regions of support of two echo patterns overlap, the patterns themselves will be highly correlated. On the other hand, if they have no overlap, the corresponding speckle patterns will be completely uncorrelated.  This is not an approximation, but the equivalent of the correlation operation in two dimensions.  It is rigorous within the constraints of the approximation underlying the \textit{k}-space representation.

\section{Imaging trade-offs}
Given the sinc$^2$-shaped response of a rectangular aperture, we observe a trade-off between lateral resolution and contrast. Commercial machines apodize the aperture to reduce the sampling of high lateral frequency components in the aperture. This reduces spatial resolution but improves contrast by reducing the energy in the side lobes of the sinc$^2$.

One could attempt to apodize the aperture to increase the sampling of high lateral frequency components in the aperture.  This would produce a sharper main lobe, but this improvement in lateral resolution would be offset by a degradation in contrast due to the emphasis of the side lobes.  This approach is sometimes used in commercial scanners in a \textbf{zoom} mode.

Another trade-off is lateral resolution versus the depth-of-field (DOF).  Given $f_{\#} = z/d$, DOF = $8~f_{\#}^2\lambda$.  Note that this dependence is with the square of the aperture size.  As shown in Figure \ref{fig:dof}, the smaller the DOF, the smaller the ``sweet-spot'' of the transmit response.  The DOF can be kept constant on receive by using dynamic aperture growth on receive.  On transmit, this can only be achieved by using multiple transmit foci.  This reduces the frame rate by a factor proportional to the number of transmit foci.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/dof.eps}
\caption{The depth of field, indicated by the arrows, is an inverse function of the aperture size.}
\label{fig:dof}
\end{figure}

The \textit{k}-space examples shown up to this point have been flat phase, but under real conditions there is often some phase profile across the aperture.  This profile may be artificial, e.g.\ to achieve focusing and steering of the beam, or it may be a natural phase aberration arising from structures in the medium.  Ideal focusing achieves perfect superposition of waves at the focus.  In a real system, the focus phase profile is quantized. This produces \textbf{phase quantization error} at the focus, as shown in Figure \ref{fig:quant_error}.  The magnitude of this error is determined by the transducer geometry and the scanner hardware.

\begin{figure}[h]
\centering
\includegraphicslarge{figures/quant_error.eps}
\caption{(Top) For a 32 element 5 MHz array with element pitch = $\lambda$ and focused at f/1, the ideal geometric delays across the array are shown by the dotted line. The actual delays achieved with 10 ns delay quantization and given the element size are shown with the solid line.  The difference between the ideal and actual delays (bottom) is known as the phase quantization error.} 
\label{fig:quant_error}
\end{figure}

While focusing in the lateral dimension of a 1-D array can be applied electronically, focusing in the elevation dimension is typically achieved with an acoustic lens, which by its nature has a fixed focal length.  Acoustic lenses are often quite severe, therefore the depth of field is poor in elevation.  As a consequence the elevation dimension spatial resolution can vary by a factor of 10 within a typical range of interest.  Clinicians are often not aware of this slice thickness problem and the resulting partial volume effects that change through depth.

Modern transducers have been designed to address imaging artifacts relating to the elevation dimensions\cite{Wildes98}. 2-D, or volumetric, arrays have a number of elements and focusing control in elevation similar to the lateral dimension, allowing more isotropic performance at the cost of a higher channel count and more control electronics. Compromises between the 1-D and 2-D array have been made by producing multiple, fixed elevation foci (1.25D), variable elevation foci restricted to the centerline of the array (1.5D) or steerable elevation foci using large elements(1.75D).
%\clearpage

\section{The target function and scattering}
When considering the pulse-echo response of the system to a scattering function at the focus, the point spread function can be used as a kernel that is convolved with a target function.  In \textit{k}-space the transfer function is multiplied by the spatial frequencies in the target function to produce the image frequency spectrum.

Target structures that scatter with spatial frequencies higher than the extent of the system response are sub-resolution and are not imaged by the system.  The \textit{k}-space representations of target functions are related to their information content. Spatial frequencies in the target function do not represent plate reflectors, but rather are sinusoidal variations in scatterer reflectivity.  For example, the glomeruli in the kidney create a $\sim3$ cycles/mm pattern. A schematic of this type of scattering structure is shown in Figure \ref{fig:period_scat}.  Resolving this structure requires spatial resolution of at least 1/6 mm in the dimension along which the structure's periodicity is oriented.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/period_scat.eps}
\caption{A medium having structure with reflectivity modulated at 3 cycles/mm in the axial dimension.}
\label{fig:period_scat}
\end{figure}

In \textit{k}-space this target is represented with an impulse-pair indicating the periodicity of the amplitude variation, as shown in Figure \ref{fig:k_deltapair}. In order to see this target with a minimal system, you have to transmit at the same
spatial frequency. 
%Even harmonics of this frequency do not work!

%Given sinusoidally distributed scatterers/acoustic impedance,%infinitely extended:
\begin{figure}[h]
\centering
\includegraphicssmall{figures/k_deltapair.eps}
\caption{A structure with sinusoidally distributed scatterers with a dominant spacing of $a$ will have an impulse-pair \textit{k}-space representation.}
\label{fig:k_deltapair}
\end{figure}

With a repeated structure, a \textbf{comb} function is produced, as shown in Figure \ref{fig:k_comb}. If this structure were rotated, one would need to launch the pulse along a similarly rotated axis in order to see the structure.

\begin{figure}[h]
\centering
\includegraphicssmall{figures/k_comb.eps}
\caption{Generally, a repeated scattering structure (i.e.\ not strictly sinusoidally distributed) is represented by a \textbf{comb} function.}
\label{fig:k_comb}
\end{figure}

Given a dominant axial scatterer spacing of $a$, consider the predicted response of the system, shown schematically in Figure \ref{fig:k_comb_system}. Observe that this type of target produces lobes in the echo frequency response. Note that the transmit-receive process introduces a factor of two in the observed axial periodicity, e.g.\ a dominant scatterer spacing of $a$ in space is observed as a $2a$ spacing in transmit-receive $k$-space. Periodicity in the scattering function produces periodicity in the echo spectrum.  This is the basis of some tissue characterization techniques, e.g.\ if pathology is correlated with tissue structure at scales to which the imaging system is sensitive, the associated echo spectra may carry useful diagnostic information\cite{Campbell84,Davros86,Insana90,Insana86,Insana92,Insana93a,Garra93}.

\begin{figure}[h]
\centering
\includegraphicslarge{figures/k_comb_system.eps}
\caption{Given a dominant axial scatterer spacing of $a$, the output of the system is the corresponding \textbf{comb} function multiplied by the response to the system. Note that the transmit-receive process introduces a factor of two in an observed axial periodicity, e.g.\ a dominant scatterer spacing of $a$ in space is observed as a $2a$ spacing in transmit-receive $k$-space.}
\label{fig:k_comb_system}
\end{figure}

%One could also take into account frequency dependent attenuation and the $f^4$ dependence of Rayleigh scattering.  
%\clearpage

\section{Frequency dependent effects}
Aside from scatterer structure, another factor that affects the target $k$-space response is attenuation. This functions as a low-pass filter in temporal frequency.  Absorption accounts for 98\% of ultrasound attenuation, while the remaining 2\% arises from scattering.  (The imaging process relies on this 2\%!)  Attenuation is typically frequency dependent, hence the parameter \textbf{frequency dependent attenuation} (FDA). The range of typical tissue attenuation values is 0.3-2 dB/cm-MHz. Its impact on the pulse-echo response is shown in Figure \ref{fig:fdatten}. Note that the fractional bandwidth for a system with a Gaussian temporal frequency response remains the same after attenuation.  While the axial resolution is not affected by attenuation, lateral resolution is degraded.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/fdatten.eps}
\caption{Generally, frequency dependent attenuation lowers the center frequency and amplitude of the pulse as it propagates through tissue. Here the temporal frequency content of the transmitted pulse is compared to that of the received echo, showing these effects. (Note that the relative amplitudes of these responses are not drawn to scale.)}
\label{fig:fdatten}
\end{figure}

The echogenicity of tissue also has a frequency dependence that can run counter to frequency dependent attenuation.  A Rayleigh scatterer is a scatterer of size $<< \lambda$. This echo intensity from this type of scatterer has an $f^4$ dependence. For example, in the 1-5 MHz range, the echo from blood is approximately 20 decibels below that from tissue, making imaging and quantification of slow flow difficult.  However, in the 20-30 MHz range, as the wavelength approaches the size of the cell, blood cells are no longer Rayleigh scatterers and the echo from blood is as bright as from the tissue.

%A real-world factor that affects ultrasound beamforming is the medium sound speed, which is typically considered to be the mean tissue value of 1540 m/s, although in actual tissue the true velocity can vary by as much as $\pm6$\.  The error between the assumed and actual velocities distorts the point spread function and thus degrades the image quality.

%Finally, the target-array geometry can produce spatial-frequency weighting that is a function of position. More widely-spaced elements see shorter path length differences among scatterers along the beam axis, as shown in Figure \ref{fig:path_length}.  Therefore scatterers are effectively closer together. Therefore one needs a higher frequency to produce the same axial interference pattern as that observed with elements at the center of the array.

%\begin{figure}[h]
%\centering
%\includegraphicssmall{figures/path_length.eps}
%\caption{In a three element system, the outer elements see an effectively smaller spacing between two on-axis scatterers than the central element. The scattered axial interference pattern observed by central element can only be observed by the outer elements by scaling the interrogation frequency proportionately.}
%\label{fig:path_length}
%\end{figure}

\section{Tissue anisotropy}
A Rayleigh scatterer is perfectly isotropic. Upon insonification, such a scatterer emits sound in all directions equally.  Large groups of these scatterers can produce a more directed echo pattern. A great number of individual scatterers aligned along a line or surface effectively form a specular reflector, a physical manifestation of the Huygen-Fresnel principle.

A real-world factor not included in our theoretical discussion is the anisotropy of tissue.  \textit{K}-space was in fact first applied in ultrasound to describe tissue anisotropy\cite{Lerner88}. Skeletal muscle has strong anisotropy due to its structure, but this type of muscle is a little interest in the ultrasound imaging. Cardiac muscle, on the other hand, shows an anisotropy that changes with both the location in the heart and over the cardiac cycle.

Miller \textit{et.\ al} \cite{Miller83} have shown that the cyclic variation in backscatter due to this anisotropy is different in healthy versus pathologic cardiac tissue. This is considered a potential means of diagnosis, although it is difficult to apply this method in a controlled manner, as the orientation of the sound beam and of the organ itself affects the results.
%\clearpage