\chapter{Additional topics}

\section{Deconvolution and super resolution methods}

Deconvolution or ``super-resolution'' methods rely mainly on two approximations. The first of these is known as the Born approximation, which disregards echoes reduced by multiple scattering from targets. The second is that the point spread function is accurately known throughout the region of interest.  

Under ideal conditions, deconvolution can work very well.  This implies that the noise is very low and that the number of targets is relatively small.  Deconvolution does not work well for larger numbers of targets under conditions of low signal-to-noise ratio, especially away from the focus and/or in the presence of phase aberration or attenuation. There performance is also degraded when physical conditions reduce the stationarity of the system's point spread function.

\section{Detection}
Detection refers to a variety of methods of estimating or approximating the RF echo envelope.  Ideally, the detection process shifts the signal band to base band, removing the carrier frequency, as shown in Figure \ref{fig:component_det}.

\begin{figure}[h]
\centering
\includegraphicssmall{figures/component_det.eps}
\caption{Through ``ideal'' demodulation, the relationships of signal frequency components to the center frequency are translated to the baseband, reestablishing these component relationships relative to DC.}
\label{fig:component_det}
\end{figure}

The simplest form of detection is AM demodulation using a full wave rectifier followed by a low pass filter.  This is shown schematically in Figure \ref{fig:square_det}, using the squaring operation to represent rectification.

\begin{figure}[h]
\centering
\includegraphicslarge{figures/square_det.eps}
\caption{If we model rectification with the squaring operation of the echo signal $e(t)$, the frequency domain representation of this detection method is shown above.  The original RF signal (left) is convolved with itself to produce base-band and double-frequency components (center). The application of a low pass filter (LPF) removes the double-frequency components, leaving the desired baseband signal (right).}
\label{fig:square_det}
\end{figure}

A more sophisticated demodulation technique is to mix the signal with that from a local oscillator.  This produces base-band and double-frequency components, the latter of which are typically filtered out using a low pass filter.  An extension of this concept, known as quadrature demodulation, is to mix the original signal with two local oscillators having the same frequency but shifted by 90$^\circ$ in phase relative to one another.  The two baseband signals that remain after low-pass filtering are often referred to as the \textbf{I} (for ``in-phase'') and \textbf{Q} (for ``quadrature'') signals. Generating both I and Q signals preserves the phase information in the original signal.

Given the trigonometric identities, 
%(XXX CHECK THIS, and IMPROVE WITH FIGURE AND PHASE TERM)
\begin{equation}
\begin{split}
\cos\alpha \sin\beta &= \frac{1}{2}[\cos(\alpha-\beta)+\cos(\alpha+\beta)]\\
\sin\alpha \sin\beta &= \frac{1}{2}[\sin(\alpha+\beta)+\sin(\alpha-\beta)]\\
\text{Consider}\\
\alpha &= \omega_{c}\\
\text{If }\beta &= \omega_{c}+\Delta\omega_{2}\\
\text{The baseband terms }&=\frac{1}{2}\cos(-\Delta\omega_{2})
=\frac{1}{2}\cos(\Delta\omega_{2})\\
\text{and }&=\frac{1}{2}\sin(-\Delta\omega_{2})
=-\frac{1}{2}\sin(\Delta\omega_{2})\\
\\
\text{If }\beta &= \omega_{c}-\Delta\omega_{1}\\
\text{The baseband terms }&=\frac{1}{2}\cos(\Delta\omega_{1})\\
\text{and }&=-\frac{1}{2}\sin(\Delta\omega_{1})\\
\end{split}
\end{equation}

Neither of these techniques achieve ``ideal'' demodulation.  While the level of energy at zero Hertz (DC) is preserved, the base-band signal away from DC contains the integration across $f$ of the product of frequencies at shift $\Delta f$:

\begin{equation}
\Bbb{F}(\Delta f) = \int^{BW/2}_{-BW/2} f_{axial}(f) f_{axial}(f+\Delta f) df
\end{equation}

The energy at $\Delta f$ contains the integral of cross products of all frequencies at this shift.  The original signal is irreversibly lost.  This process mixes spatial frequency components across the bandwidth.

The most accurate demodulation technique is to use the Hilbert operator or the Hilbert transform to generate the quadrature signal at the RF center frequency.  This operation does not assume any particular center frequency, and is thus immune to discrepancies between the local oscillator's frequency and that of the echo signal. The echo envelope is then found by calculating the magnitude of the complex signal formed by combining the original RF signal with its synthesized quadrature signal. With this method, no filtering is necessary.

%If a pulse with original bandwidth of 5 MHz is passed through a 2.5
%MHz bandwidth filter, blurring results.  This is equivalent to
%convolution with a sinc function in the time domain. In fact we only
%blur the original signal across a pulse length.  Therefore the
%detection filter is blurring data already blurred by the pulse
%shape. In most methods of envelope detection, the instantaneous phase
%of the original signal is lost, as is the center frequency.

%\section{Elastography imaging}
%Elastography refers to the imaging of the mechanical elastic properties of tissue\cite{Gao96,Sarvazyan95}. These methods are based on vibrating or compressing the tissue while imaging the tissue with ultrasound. This approach grew from the dynamic palpation of the body during imaging.
%
%Various methods for creating elastic waves or motion in a tissue are under investigation, including an internally or externally applied vibrator, an externally-applied compression paddle, or pumping waves using low frequency sound.  Motion under vibration is imaged using Doppler techniques or speckle-tracking techniques.  Standing waves are a problem, especially with strong vibration necessary for the targets.
%
%Compression based techniques typically used some kind of correlation search before and after compression.  Some error is produced in these techniques by the decorrelation of speckle during compression. Decorrelation during compression results in part from scatterers moving into or out of the resolution volume, and partly from changes in the geometric relationships among particles within the resolution volume\cite{Meunier95,Insana97}.

\section{Limitations of \textit{k}-space}
In closing, it is important to reiterate the various limitations of \textit{k}-space. The most obvious limitation is that it is a linear systems approach that follows from the Fraunhofer approximation, and thus can only be applied under conditions of linear propagation and within the constraints of that approximation.  Thus \textit{k}-space cannot be applied in analyzing non-linear imaging modalities or the near field. Also, modeling the interaction between an imaging system and scatterers is only straightforward for targets that can be represented as point targets or collections of point targets, and under conditions justifying the Born approximation. There is no obvious means to represent non-Rayleigh scatterers, or to consider conditions in which the Born approximation is unjustified, such as in blood with physiological levels of hematocrit, or a bolus of a microbubble ultrasonic contrast agent. Finally, some \textit{k}-space operations cannot be applied in the analysis of a spatially shift-variant system, such as in a complex flow field in which shear and turbulence are present that are warping the point-spread function as a function of location.

Despite these limitations, we hope this text has demonstrated the utility of a \textit{k}-space approach in understanding and analyzing many aspects of ultrasound imaging systems and beamforming techniques.
