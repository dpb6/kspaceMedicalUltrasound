\chapter{A brief introduction to ultrasound}

Medical ultrasound, also called sonography, is a mode of medical imaging that has a wide array of clinical applications, both as a primary modality and as an adjunct to other diagnostic procedures. The basis of its operation is the transmission of high frequency sound into the body followed by the reception, processing, and parametric display of echoes returning from structures and tissues within the body. Ultrasound is primarily a tomographic modality, meaning that it presents an image that is typically a cross-section of the tissue volume under investigation. It is also a soft-tissue modality, given that current ultrasound methodology does not provide useful images of or through bone or bodies of gas, such as found in the lung and bowel. Its utility in the clinic is in large part due to three characteristics. These are that ultrasound a) is a real-time modality, b) does not utilize ionizing radiation, and c) provides quantitative measurement and imaging of blood flow.

The purpose of this document is to present the ultrasound researcher with methods for characterizing and analyzing ultrasound imaging systems.  The central approach is based on linear systems theory, i.e.\ the use of spatial- and temporal-frequency domain representations of ultrasound system impulse responses and scattering functions to understand and analyze both conventional and hypothetical imaging methods. Spatial frequency domain, or \textbf{\textit{k}-space}, analysis can be a powerful, intuitive tool for the instruction, investigation, and development of ultrasound beamforming techniques\cite{Lerner88,Walker98}.

This document is not intended as a general introduction to ultrasound, either in terms of physical acoustics or clinical practice.  The authors have included references in the text to some of the many sources of information on these and other related topics that are beyond the scope of this document.

\section{Resolution, beamforming and the point spread function}
% beginning of PhD dissertation excerpt, from /home/mea1/tex/phd/back.tex
A typical transducer uses an array of piezoelectric elements to transmit a sound pulse into the body and to receive the echoes that return from scattering structures within.  This array is often referred to as the imaging system's \textbf{aperture}. The transmit signals passing to, and the received signals passing from the array elements can be individually delayed in time. This is done to electronically steer and focus each of a sequence of acoustic pulses through the plane or volume to be imaged in the body.  This produces a 2- or 3-D map of the scattered echoes, or \textbf{tissue echogenicity} that is presented to the clinician for interpretation.  The process of steering, focusing and combining these acoustic pulses to form an image is known as \textbf{beamforming}.  This process is shown schematically in Figure~\ref{fig:beamform}.

\begin{figure}[h!]
\centering
\includegraphicsmed{figures/beamform.eps}
\caption{A conceptual diagram of phased array beamforming.  (Top) Appropriately delayed pulses are transmitted from an array of piezoelectric elements to achieve steering and focusing at the point of interest. (For simplicity, only focusing delays are shown here.) (Bottom) The echoes returning are likewise delayed before they are summed together to form a strong echo signal from the region of interest.}
\label{fig:beamform}
\end{figure}

The ability of a particular ultrasound system to discriminate closely spaced scatterers is specified by its spatial resolution, which is typically defined as the minimum scatterer spacing at which this discrimination is possible.  The system resolution has three components in Cartesian space, reflecting the spatial extent of the ultrasound pulse at the focus.  The coordinates of this space are in the axial, lateral, and elevation dimensions.  The axial, or \textbf{range}, dimension indicates the predominant direction of sound propagation, extending from the transducer into the body.  The axial and the lateral dimension together define the tomographic plane, or slice, of the displayed image.  These dimensions relative to the face of a linear array transducer are shown in Figure~\ref{fig:cartes}. The elevation dimension contains the slice thickness.

\begin{figure}[htbp]
\centering
\includegraphicsmed{figures/cartes.eps}
\caption{A diagram of the spatial coordinate system used to describe the field and resolution of an ultrasound transducer array.  Here the transducer is a 1-D array, subdivided into elements in the lateral dimension.  The transmitted sound pulse travels out in the axial dimension.}
\label{fig:cartes}
\end{figure}

A modern ultrasound scanner operating in brightness mode, or \textbf{B-mode}, presents the viewer with a gray-scale image that represents a map of echo amplitude as a function of position in the region being scanned.  In B-mode the ultrasound system interrogates the region of interest with wide bandwidth sound pulses.  Such a pulse from a typical array is shown in Figure~\ref{fig:psf_shade}.

\begin{figure}[htbp]
\centering
\includegraphicslarge{figures/psf_shade.eps}
\caption{The acoustic pulse from a typical array (7.5 MHz, 60\% bandwidth, 128 elements of width equal to the wavelength), shown at the acoustic focus. The pulse is displayed as a map of pressure amplitude and is traveling in the positive direction along axial dimension.
%The pulse is traveling to the right and toward the viewer.
}
\label{fig:psf_shade}
\end{figure}

The acoustic pulse in Figure~\ref{fig:psf_shade} is shown as a function of acoustic pressure over the lateral and axial dimensions. In fact the pulse is a three-dimensional function, with extent in elevation as well.  In the terminology of linear systems theory it is the impulse response of the system, and the response of the ultrasound system at the focus is fully characterized by this function.  As it represents the output of the ultrasound system during interrogation of an ideal point target, it is also known as the system's \textbf{point spread function} (PSF).  The character of the PSF in the axial dimension is determined predominantly by the center frequency and bandwidth of the acoustic signal generated at each transducer element, while its character in the lateral and elevation dimensions is determined predominantly by the aperture and element geometries and the beamforming applied. The term PSF is often used to refer to two-dimensional representations of the system response in pressure amplitude versus space, such as that shown in Figure~\ref{fig:psf_shade}, with the implicit understanding that the actual response has three-dimensional extent.
  
In analyzing hypothetical ultrasound systems, predicting the form of the PSF is critical.  However, the analytic solution for the PSF for an arbitrary array geometry is usually intractable.  Throughout this document, an acoustic field simulation program developed by Jensen and Svendsen was used to predict the acoustic field under the various conditions and array geometries of interest\cite{Jensen92}.  This program is based on a method developed by Tupholme and Stephanishen\cite{Tupholme69,Stephanishen71,Stephanishen71a}.  It calculates the convolution of a transmit excitation function, such as a sine wave with Gaussian envelope, with the spatial impulse response of the transducer.  The spatial impulse response is the hypothetical pressure pattern created upon excitation of the array with a perfect impulse.  The spatial impulse response is not a physically realizable, but serves as a useful calculation tool in this context.  This method can accommodate arbitrary geometries by division of the aperture into smaller, rectangular elements.  The spatial impulse response for each element is calculated separately, and then these solutions are combined by superposition to produce that for the entire aperture.

The three components of spatial resolution define a what is called the resolution volume.  A modern, high frequency ultrasound transducer has a resolution volume at the focus that is on the order of 300 x 300 x 1000 $\mu$m axially, laterally, and in elevation, respectively.  The evolution of the dimensions of the acoustic pulse from a typical array (7.5 MHz, 60\% bandwidth, 128 elements of width equal to the wavelength, or \textbf{$\lambda$ -pitch}) as it passes through the focal plane with fixed transmit and receive focus is shown in Figure~\ref{fig:psf_patch}.  This figure plots the -6 dB amplitude contour of the PSF in a sequence of slices.  Each slice defines a plane in the lateral-elevational dimensions, and the slices are spaced in the axial dimension.  These contours demonstrate how the PSF is most compact at the focus, and also show the resolution mismatch between the lateral and elevation dimensions that is typical of a 1-D array.  The axial dimension of the resolution volume is not displayed in this plot.

\begin{figure}[h!]
\centering
\includegraphicsmed{figures/psf_patch.eps}
\caption{This diagram shows how the spatial resolution of the acoustic pulse in the lateral and elevation dimensions changes as it travels in the axial dimension through the focal plane.  These acoustic pressure amplitude contours are -6 dB relative to the peak amplitude within each slice of the point-spread function (PSF) as it propagates. Dimensions are relative to the focal point.\\}
\label{fig:psf_patch}
\end{figure}
% end of PhD dissertation excerpt

The \textbf{sound} in ultrasound is a physical longitudinal wave. The compression and rarefaction of the medium at the wavefront causes particle translations on the order of microns.  The tissue at the cellular level is perturbed on a bulk level, i.e.\ the wavelength is much greater than the size of cells.

Here are some numbers of interest to put ultrasound in perspective. At 1 MHz, 100 mW/$cm^2$ (FDA upper acoustic power limit):

\begin{tabbing}
\hskip 0.5in\=Wavelength\hskip 1.5in\=		1.5 mm\\
\>Phase velocity\>		1540 m/s = 1.54 mm/$\mu$s\\
\>Peak particle displacement\>	0.0057 $\mu$m\\
\>Peak particle velocity\>	3.8 cm/sec\\
\>Peak particle acceleration\>	22,452 g\\
\>Peak pressure\>		1.8 atm\\
\>Radiation force\>		0.007 g/cm$^2$\\
\>Heat equivalent\>		0.024 cal/sec cm$^2$ (total absorption)\\
\end{tabbing}

\section{Sound wave propagation as a linear process \label{sec:waveeq}}
From Insana and Brown, we draw a concise opening paragraph:
\begin{quote}
Fluids have elasticity (compressibility $\kappa$) and inertia (mass density $\rho$), the two characteristics required for wave phenomena in a spatially distributed physical system whose elements are coupled. Elasticity implies that any deviation from the equilibrium state of the fluid will tend to be corrected; inertia implies that that the correction will tend to overshoot, producing the need for a correction in the opposite direction and hence allowing for the possibility of propagating phenomena -- acoustic pressure waves\cite{Insana93d}. 
\end{quote}

Throughout this text we will describe and analyze ultrasound systems in the context of linear acoustics, meaning that pressure wave fields considered are assumed to represent solutions of the the linear wave equation.  There are many forms of the wave equation, one of which is

\begin{equation}
%\begin{split}
\frac{\partial^2 p}{\partial z^2} = \rho_0 \kappa \frac{\partial^2 p}{\partial t^2} 
%\end{split}
%\label{}
\end{equation}

where $p$ is pressure, $\rho_0$ is the medium density at equilibrium, and $\kappa$ is the compressibility. This is a one-dimensional form of the wave equation in time $t$ and position $z$.

The utility of \textit{k}-space in ultrasound research grows from the fact that sinusoidal pressure waves both satisfy the multi-dimensional linear wave equation and form the basis set of the multi-dimensional Fourier transform.  Thus any system operating under linear conditions has a \textit{k}-space representation.

In the context of three-dimensional \textit{k}-space, the general form of the basis function is a continuous plane wave. An example in analytic form would be 

\begin{equation}
%\begin{split}
p(x,y,z,t) = P_0 \cos(2 \pi f t - kz)\text{~~where~~} k = 2 \pi f / c
%\end{split}
%\label{}
\end{equation}

for a plane wave of amplitude $P_0$ and wavenumber $k$ traveling in the positive direction along the $z$ axis in a medium of sound speed $c$. It is important to note that for this four-dimensional basis function to be represented in 3-D \textit{k}-space, one of the variables must be held constant (usually time $t$). We can represent any physically realizable solution of the linear wave equation with a superposition of such plane waves at the appropriate spatial frequencies, amplitudes, and phases.

\section{The scattering and reflection of sound}
Medical ultrasound imaging relies utterly on the fact that biological tissues scatter or reflect incident sound.  Although the phenomenon are closely related, in this text \textbf{scattering} refers to the interaction between sound waves and particles that are much smaller than the sound's wavelength $\lambda$, while \textbf{reflection} refers to such interaction with particles or objects larger than~$\lambda$.

The scattering or reflection of acoustic waves arise from inhomogeneities in the medium's density and/or compressibility. Sound is primarily scattered or reflected by a discontinuity in the medium's mechanical properties, to a degree proportional to the discontinuity. (By contrast, continuous changes in a medium's material properties cause the direction of propagation to change gradually.) The elasticity and density of a material are related to its sound speed, and thus sound is scattered or reflected most strongly by significant discontinuities in the density and/or sound speed of the medium. A thorough review of the mechanisms of scattering is provided by Insana and Brown\cite{Insana93d}.

\subsection{Scatterer sizes less than and equal to the wavelength}
Consider a medium subjected to acoustic insonification, and a particle in this medium that is much smaller than the wavelength of the sound. If this particle is of the same density and compressibility as the medium, it scatters no sound at all. If its density is the same but its compressibility is different, it will react to the surrounding pressure variation by radially expanding and contracting differently than the surrounding medium. This type of scatterer gives off \textbf{monopole radiation}, shown in Figure \ref{fig:monopole}.  If these conditions are reversed, i.e.\ the particle's compressibility is the same as the surrounding medium but its density is different, the particle's motion in response to the incident wave will not equal that of the medium, and it will move back and forth in relation to the medium.  This type of scattering gives off \textbf{dipole radiation}, also shown in Figure \ref{fig:monopole}.  Most real sub-wavelength scatterers emit some combination of monopole and dipole radiation, although one may dominate significantly over the other.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/monopole.eps}
\caption{(Left) The pressure amplitude pattern of monopole radiation from a ``compressibility'' scatterer is isotropic. (Right) The corresponding pattern of a ``density'' scatterer is highly directional, corresponding to two closely-spaced monopoles operating out of phase. The pattern exhibits a $180^\circ$ phase shift along the axis of sound propagation, and a null surrounding the scatterer and normal to this axis.}
\label{fig:monopole}
\end{figure}

The radiation pattern becomes more complex as the scatterer size approaches the wavelength of the incident sound. The analytic solution for the radiation pattern from a spherical elastic scatterer in a fluid was first described by Faran\cite{Faran51}. The radiation pattern of such a scatterer is dependent on the material properties of the sphere and the medium and on the sphere's radius. Also, the result depends on whether one includes the effects of sound penetrating into the sphere and reverberating, which distinguishes the ``elastic'' from the ``inelastic'' solution. Faran solutions for radiation magnitude vs.\ scattering angle are shown in Figures \ref{fig:ka01_1} and \ref{fig:ka3_5} for a range of sphere radii, parameterized by the sphere radius $a$ and the wavenumber of insonification $k = 2\pi/\lambda$, and using the material properties of crown glass ($c$ = 5100 m/s, $\rho$ = 2.4 gm/cm$^3$, $\sigma$ = 0.24) in 20$^\circ$ water\cite{Selfridge85}.  Faran defines the scattering angle such that the sound source is at 180$^\circ$. These figures show that as the scatterer radius is increased up to and beyond the wavelength of the sound, the radiation pattern becomes progressively more directional.

\begin{figure}[h!]
\centering
\includegraphicsmed{figures/ka01_1.eps}
\caption{Angular scattering from elastic (solid) and inelastic (dashed) spheres of circumference~$\leq~\lambda$. The insonification source is located at 180$^\circ$.}
\label{fig:ka01_1}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphicsmed{figures/ka3_5.eps}
\caption{Angular scattering from elastic (solid) and inelastic (dashed) spheres of circumference~$>~\lambda$. The insonification source is located at 180$^\circ$.}
\label{fig:ka3_5}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphicslarge{figures/ka_ray.eps}
\caption{(Left) The backscatter amplitude for a sphere for $k a < 1$ (solid) is compared to $(k a)^2$ (dashed), showing that for scatterers smaller than the wavelength, echo amplitude as a $f^2$ dependence, corresponding to an intensity dependence of $f^4$, a characteristic of Rayleigh scattering. (Right) The backscatter amplitude for a sphere as a function of frequency is compared for the elastic (dashed) and inelastic (solid) cases, showing how internal reverberation of sound within a scatterer can create peaks and nulls in the scattering spectrum.}
\label{fig:ka_ray}
\end{figure}

We can make several other observations using the Faran model. In Figure \ref{fig:ka_ray} we consider the magnitude of the sphere's echo back at the source as a function of $k a$, the product of the frequency and the sphere's radius.  In the left half of the figure, we consider $k a < 1$, and observe that the echo magnitude for this condition (solid line) is roughly comparable to $(k a)^2$ (dashed line).  In other words, the echo magnitude of scatterers significantly smaller than the wavelength has an $f^2$ dependence.  When echo magnitude $|A|$ is converted to intensity $|A|^2$, this dependence is approximately $f^4$.  Scatterers that are much smaller than the wavelength are known as \textbf{Rayleigh scatterers}, and are generally considered to have an $f^4$ intensity dependence.

Also shown in Figure \ref{fig:ka_ray} is a graph of the backscatter echo magnitude (i.e.\ scattering angle = 180$^\circ$) for $0< k a < 10$. As before, the material properties used in the model are those for crown glass. The elastic (dashed) and the the inelastic (solid) solutions are contrasted. For a fixed diameter $a$, this Faran solution corresponds to a scatterer ``frequency response''. It is seen that when the solution includes the effects of sound penetrating into the sphere and reverberation within the sphere, this spectrum develops complex nulls and peaks. Thus reverberation of sound within a scatterer results in a echo response that can be highly frequency dependent, and markedly different from the response predicted \textit{without} accounting for these effects. On the other hand, the inelastic response is adequate for materials with density and/or sound speed \textit{much} greater than those of the surrounding fluid. Comprehensive tables of the acoustic parameters of materials and of biological tissues can be found in \cite{Selfridge85}, \cite{Goss78}, and \cite{Goss80}.

\subsection{Reflecting structures larger than the wavelength}
Tissue structures within the body feature boundaries on a scale much larger than the wavelength.  Prominent \textbf{specular} echoes arise from these boundaries. The acoustic properties of tissues are often characterized using the concept of \textbf{acoustic impedance} $Z$.  $Z=\rho_{0} c = \sqrt{\rho_{0} c_{elas}}$, where $\rho_{0}$ is the density of the tissue, $c$ is the sound speed, and $c_{elas}$ is the tissue elasticity. When a wave is directly incident on a boundary between two media with acoustic impedances $Z_{1}$ and $Z_{2}$, the ratio of reflected  to incident pressure is predicted by the reflection coefficient $R$, defined as:

\begin{equation}
%\begin{split}
R = \frac{Z_{2}-Z_{1}}{Z_{2}+Z_{1}}
%\end{split}
\label{eq:refcoef}
\end{equation}

It is important to note that in ultrasound, as in optics, tissue boundaries can also give rise to refraction that can produce image artifacts, steering errors, and aberration of the beam. A more general form of Equation \ref{eq:refcoef} describes reflection and refraction as a function of incident angle, where the incident and transmitted angles $\theta_{i}$ and $\theta_{t}$, shown in Figure \ref{fig:snell}, are related by Snell's Law:

\begin{equation}
%\begin{split}
R = \frac{(Z_{2}/\cos\theta_{t})-(Z_{1}/\cos\theta_{i})}
              {(Z_{2}/\cos\theta_{t})+(Z_{1}/\cos\theta_{i})}
              \text{~~where~~}
               \frac{\sin\theta_{i}}{\sin\theta_{t}} = 
               \frac{\lambda_{1}}{\lambda_{2}}=
               \frac{c_{1}}{c_{2}}
%\end{split}
%\label{}
\end{equation}

\begin{figure}[h]
\centering
\includegraphicsxsmall{figures/snell.eps}
\caption{The geometry of reflection and refraction at a boundary between media with different sound speeds (see text).}
\label{fig:snell}
\end{figure}

%A more general observation follows from these relationships.  
It is obvious that ultrasound imaging relies on reflected sound to create images of tissue boundaries and structures.  A more subtle fact is that it relies on the transmitted component of the wave as well. If all the transmitted sound energy is reflected at a particular interface due to a particularly severe acoustic impedance mismatch, no sound penetrates further to illuminate underlying structures. A good example of such a mismatch in the body is the boundary between tissue and air, such as in the lungs. This accounts for the inability of ultrasound to image tissues within or lying under the lung.

\subsection{Coherent and incoherent scattering}
In this section we have discussed scattering by individual scatterers and reflection by specular targets. A more typical type of scattering in the bodies arises from a diffuse population of sub-resolution scatterers.  If the arrangement of these scatterers is spatially random, the resulting \textbf{incoherent} scattering gives rise to \textbf{speckle noise}. If the scatterers have a periodic arrangement, a \textbf{coherent} component is introduced, producing periodicity in the echo spectrum.  Both incoherent and coherent scattering will be discussed further in following sections.