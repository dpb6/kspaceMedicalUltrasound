\chapter{Second Order Speckle Statistics}
\section{Speckle and the phase spectrum}
At the focus, we consider the phase of \textit{k}-space response of the ultrasound system to be flat.  If we multiply this flat phase \textit{k}-space response by the complex spectrum of speckle, we observe no net change in the amplitude spectrum, but the phase spectrum takes on the characteristics of the speckle, as shown in Figure \ref{fig:flat_phase}. The phase and amplitude characteristics of the transmit, scattering, and receive events can be represented as three random walks in complex amplitude space.  The randomization of phase in any of these walks results in the production of a speckle pattern.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/flat_phase.eps}
\caption{The complex spectra for a point target (top) and for a speckle target (middle) in the lateral \textit{k}-space dimension. The observed (imaged) speckle pattern (bottom) reflects the spatial frequency response of the imaging system.}
\label{fig:flat_phase}
\end{figure}

It is also important to note that the randomization of phase shown in Figure \ref{fig:flat_phase} can also be produced by phase errors or phase aberration in either the aperture function or the propagation path. In this manner, it is possible for a point target that has flat phase to appear as a localized speckle pattern to the imaging system, as its echo has taken on the phase characteristics of speckle across the aperture.

In Field II simulations, it is common to generate speckle patterns by randomizing either the location or amplitude of scatterers. Modifying the location directly produces the phase characteristics described above. Modifying the amplitude treats each scatterer itself as a lumped model of sub-resolution scatters with the amplitude representing the magnitude of the phasor sum. Because the individual phases are not retained, this method does not converge in as few scatterers as when randomizing location but will produce the desired result.

\section{The autocorrelation of speckle}
In considering the autocorrelation operation applied to speckle, recall that the power spectrum is the Fourier transform of the autocorrelation operation, and that neither function has a phase component.  Therefore, we expect the lateral and axial autocovariance functions of a point target (that has a target spectrum with flat phase) and that averaged over many speckle patterns (that have target spectra with random phase) to be the same, since the autocorrelation function is phase independent. These functions for RF and detected speckle signals are shown in Figure \ref{fig:autocorrelation}.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/autocorrelation.eps}
\caption{The autocorrelation functions of the RF signal (solid line) and the detected signal (dashed line) for the axial (top) and lateral (bottom) dimensions. In the axial case, both the RF correlation and the envelope of this correlation are shown using a solid line.}
\label{fig:autocorrelation}
\end{figure}

Any power spectrum or autocorrelation function estimated from a limited window of speckle data will have random fluctuations due to the stochastic nature of the speckle pattern. Thus when these functions are actually estimated from speckle data, one must average over many independent realizations of the speckle to produce the expected result.

%The detection process decreases the bandwidth of the echo signal.  
The ringing in the autocorrelation function of the original RF echo signal means that its main lobe width cannot be used as a meaningful index of spatial resolution.  There is no ringing in the lateral dimension.

The widths of the detected signal's lateral and axial autocorrelation functions reflect the system resolution in these dimensions.  We use the following rules of thumb:
\begin{itemize}
\item lateral resolution = $\lambda z/D$
\item axial resolution $\approx \frac{1}{2}$ pulse length ($\approx \lambda$ for 100\% relative bandwidth)
\end{itemize}

In general, lateral resolution is worse than axial resolution, and gets worse with range.  Therefore, the system resolution is anisotropic.  Given the Gaussian axial spatial frequency response and typical attenuation, the shape of the echo envelope remains constant. Thus axial resolution is constant over range even though the echo pulse's mean frequency decreases. 

\section{Important speckle references}
The speckle statistics presented here follow Goodman\cite{Goodman85}. Other fundamental discussions on speckle include:

\begin{tabular}{l l l l}
Citation & Authors & Year & Description\\
\cite{Goodman84} & Goodman & 1984 & A good source on laser optics\\
\cite{Burckhardt78} & Burckhardt & 1978 & The first discussion of ultrasound speckle using statistical optics\\
\cite{Wagner87a} & Wagner et al. & 1987 & Advanced papers on the statistics of speckle\\
\cite{Thijssen90} & Thijssen et al. & 1990 & Advanced papers on the statistics of speckle\\
\cite{Wagner88} & Wagner et al. & 1988 & A classic paper on the second-order statistics of speckle\\
\cite{Trahey88} & Trahey and Smith & 1988 & The effect of aberration on speckle statistics\\
\cite{Smith88} & Smith et al. & 1988 & The effect of aberration on speckle statistics\\
\cite{Insana86} & Insana et al. & 1986 & A discussion of Rician statistics in tissue characterization\\
\cite{Trahey86} & Trahey et al. & 1986 & Investigations of spatial compounding\\
\cite{ODonnell88} & O'Donnell et al. & 1988 & Investigations of spatial compounding\\
\cite{Melton84} & Melton et al. & 1984 & Frequency compounding\\
\cite{Trahey86} & Trahey et al. & 1986 & Frequency compounding\\
\end{tabular}
%\clearpage
