\chapter{First order speckle statistics}
\section{Magnitude (using Trahey's notation)}
 Given a monochromatic carrier signal described with the phasor form $e^{j2\pi f_{c}t}$, where $f_{c}$ is the center frequency, we introduce a formulation of the RF speckle pattern in the analytic form:
 
 \begin{equation}
P(x,y,z;t)=A(x,y,z)e^{j2 \pi f_{c}t}
\end{equation}

where $A(x,y,z)$ is the complex phasor amplitude. $A(x,y,z)$ can be decomposed into magnitude and phase:

\begin{equation}
A(x,y,z)=|A(x,y,z)|e^{j\theta(x,y,z)}
\end{equation}

The intensity of the phasor is given by:

\begin{equation}
I(x,y,z)=|A(x,y,z)|^2
\end{equation}

Each scatterer contributes to the complex phasor amplitude:

\begin{equation}
A(x,y,z)=\sum_{k=1}^{N}\frac{1}{\sqrt{N}}a_{k}(x,y,z) = 
\frac{1}{\sqrt{N}}\sum_{k=1}^{N}|a_{k}|e^{j\theta_{k}}
\end{equation}

Assumptions:

\begin{itemize}
\item The amplitude $a_{k}/\sqrt{N}$ and phase $\theta_{k}$ of the $k^{th}$ scatterer are statistically independent of each other and of those of all other scatterers.
\item The phases of the scatterers are uniformly distributed over $[-\pi,\pi]$.
\end{itemize}

Now we can calculate an assortment of expected values:

\begin{equation}
\begin{split}
A^r =\text{Real}(A) &=
\frac{1}{\sqrt{N}}\sum_{k=1}^{N}|a_{k}|\cos(\theta_{k})\\
A^i = \text{Imaginary}(A) &=
\frac{1}{\sqrt{N}}\sum_{k=1}^{N}|a_{k}|\sin(\theta_{k})\\
\langle A^r \rangle &= \frac{1}{\sqrt{N}}\sum_{k=1}^{N}\big{\langle}
|a_{k}|\cos(\theta_{k})\big{\rangle} =\frac{1}{\sqrt{N}}\sum_{k=1}^{N}\big{\langle}
|a_{k}|\big{\rangle}\big{\langle}\cos(\theta_{k})\big{\rangle} = 0\\
\langle A^i \rangle &= \frac{1}{\sqrt{N}}\sum_{k=1}^{N}\big{\langle}
|a_{k}|\sin(\theta_{k})\big{\rangle} =\frac{1}{\sqrt{N}}\sum_{k=1}^{N}\big{\langle}
|a_{k}|\big{\rangle}\big{\langle}\sin(\theta_{k})\big{\rangle} = 0\\
\big{\langle} |A^r|^{2} \big{\rangle} &= \frac{1}{N}\sum_{k=1}^{N}\sum_{m=1}^{N}\big{\langle}
|a_{k}a_{m}|\big{\rangle}\big{\langle}\cos(\theta_{k})\cos(\theta_{m})\big{\rangle} =
\frac{1}{N}\sum_{k=1}^{N}\frac{\big{\langle}|a_{k}|\big{\rangle}^2}{2}\\
\big{\langle} |A^i|^{2} \big{\rangle} &= \frac{1}{N}\sum_{k=1}^{N}\sum_{m=1}^{N}\big{\langle}
|a_{k}a_{m}|\big{\rangle}\big{\langle}\sin(\theta_{k})\sin(\theta_{m})\big{\rangle} =
\frac{1}{N}\sum_{k=1}^{N}\frac{\big{\langle}|a_{k}|\big{\rangle}^2}{2}\\
\langle A^r A^i \rangle &= 0
\end{split}
\end{equation}
 
 Through the application of the Central Limit Theorem, $A(x,y,z)$ has a complex Gaussian PDF, with joint real and imaginary parts:
 
\begin{equation}
\begin{split}
p_{r,i}(A^rA^i)&=\frac{1}{2\pi\sigma^2}\exp\bigg{(}
-\frac{|A^r|^2+|A^i|^2}{2\sigma^2}
\bigg{)}\\
\text{where }
\sigma^2&=\underset{N\rightarrow\infty}{\text{lim}}\frac{1}{N}
\sum_{k=1}^{N}\frac{|a_{k}|^2}{2}
\end{split}
\end{equation}

For large $N$ the Rayleigh PDF of the phasor magnitude is found to be:

\begin{equation}
p(V)=
\begin{cases}
\frac{V}{\sigma^{2}N}\exp\Big{(}-\frac{V^2}{2\sigma^{2}N}\Big{)}& V \geq
0,\\
0 &\text{otherwise.}
\end{cases}
\end{equation}

The first order statistics for magnitude are:

\begin{equation}
\begin{split}
\mu_{V} &= \langle V \rangle = \bigg{(}\frac{N\pi}{2}
\bigg{)}^{1/2}\sigma\\ 
\sigma_{V}^{2} &= \big{\langle} (V-\mu)^2 \big{\rangle} =
[2-\pi/2]N\sigma^2\\
\therefore \mu_{V}/\sigma_{V} &= 1.91\text{, equivalent to 5.6 dB SNR}
\end{split}
\end{equation}

\section{Intensity (using Goodman's notation)}
$|A(x,y,z)|^2$ has the PDF:
\begin{equation}
p(I)=p(V^2)=
\begin{cases}
\frac{1}{2 \sigma^{2}}\exp\Big{(}-\frac{I}{2\sigma^{2}}\Big{)}& I \geq
0,\\
0 &\text{otherwise.}
\end{cases}
\end{equation}
and statistics:\hskip 2.5cm $\sigma_{I} = \mu_{I} = 2 \sigma^{2}$

\section{A review of random variables\label{random_review}}

 Given two random variables $x$ and $y$:
\begin{equation}
\begin{split}
\text{The mean of } x &= \mu_{x} = \langle x \rangle\\
& \text{where
}\langle \rangle \text{ denotes ``expected value of''.} \\
\text{The variance of } x &= \sigma_{x}^2 = \big{\langle} (x-\mu_{x})^2 \big{\rangle}\\
\text{For } x \text{ zero-mean, } \sigma_{x}^2 &= \langle x^2 \rangle\\
\text{The standard deviation of } x &= \sigma_{x}\\
\\
\text{The covariance of } x \text{ and } y &= \Bbb{C}_{x,y} = 
\big{\langle} (x-\mu_{x})(y-\mu_{y})^{\ast} \big{\rangle}\\
\text{The normalized correlation coefficient of } x \text{ and } y &= \rho = 
\frac{\Bbb{C}_{x,y}}{\sigma_{x}\sigma_{y}}\\
\mu_{x+y} &=\mu_{x} +\mu_{y}\\
\sigma_{x+y}^2&=\sigma_{x}^2+\sigma_{y}^2 + 2\rho\sigma_{x}\sigma_{y}\\
\text{For } x,y \text{ independent, }\sigma_{x+y}^2&=\sigma_{x}^2+\sigma_{y}^2\\
\text{For } x,y \text{ independent, }\langle x y \rangle &=
\langle x \rangle \langle y \rangle 
\end{split}
\end{equation}
