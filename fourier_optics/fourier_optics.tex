\chapter{Ultrasound and Fourier optics}

\section{Propagation as a linear operation}
As discussed in Section \ref{sec:waveeq}, linear propagation is governed by the linear wave equation. 
%(See the Helmholtz equation.)  
An important consequence of representing propagation of sound as a linear process is that we can apply the superposition principle.

The \textbf{Huygen-Fresnel principle} states that wavefronts can be decomposed into a collection of point sources, each the origin of a spherical, expanding, wave that can be represented as a free space Green's function.  This concept underlies the derivation of an important tool, the Fraunhofer Approximation.

\section{The significance of the Fraunhofer approximation}

The \textbf{Fraunhofer approximation} (FA) plays a pivotal role in our exploration of ultrasound \textit{k}-space.  In a nutshell, this well-known expression from the optics literature states that the far-field complex amplitude pattern produced by a complex aperture amplitude function is approximately equal to 2-D Fourier transform of that function. Applied to ultrasound, this approximation states that the ultrasound beam's pressure amplitude pattern can be estimated by taking the 2-D Fourier transform of the transducer aperture. Naturally, this approximation is based on several assumptions and requirements that constrain its application to the far field of an unfocused transducer or the focal plane of a focused transducer. While the approximation is based on a single frequency, or \textbf{monochromatic} solution, we can extend its application to broadband cases through superposition. As we shall see, the pattern is parameterized by the frequency of the wave emitted at the aperture.

As long as we don't violate the assumptions made in formulating the FA, this powerful approximation allows us to extend our intuition regarding linear systems to the study of ultrasound beamforming. Restricting the discussion for a moment to the lateral and axial dimensions, the most obvious example of an aperture function is the rectangular aperture of a linear array lying along the lateral coordinate axis, emitting a single frequency of sound.  Representing this aperture function as a \textbf{rect} and applying the FA, we can guess that the ultrasound pressure amplitude pattern in the far field of this aperture is a \textbf{sinc} function.

\section{The development of the Fraunhofer approximation}
%\textbf{N.B.:} These pages are intended as a quick overview or %refresher.  
The complete theoretical development of the FA, from which these excerpts are drawn, can be found in Goodman's \textit{Introduction to Fourier Optics}\cite{Goodman68}.

\begin{figure}[bth]
\centering
\includegraphicsmed{figures/fraun_geom.eps}
\caption{Coordinate system used in the derivation of the Fraunhofer approximation.}
\label{fig:fraun_geom}
\end{figure}

The coordinate system used is this discussion is shown schematically in Figure \ref{fig:fraun_geom}.  We consider an arbitrary aperture $\Sigma$ in the aperture plane $(x_{1},y_{1})$ emitting monochromatic radiation of wavenumber $k$, spatially weighted with a complex amplitude function $U(x_{1},y_{1})$. The object is to determine the complex field pattern at an observation plane $U(x_{0},y_{0})$.  The geometric path length between any two points $P_{0}$ and $P_{1}$ is
simply:

\begin{equation}
r_{01}=\sqrt{z^2+(x_{1}-x_{0})^2+(y_{1}-y_{0})^2}
\label{eq:r01}
\end{equation}

Consider the application of Huygen's principle to the aperture, i.e.\ that this arbitrary, spatially extended source is represented by an infinite number of point sources lying across $\Sigma$, each with a particular amplitude and phase defined by $U(x_{1},y_{1})$. The amplitude $U(P_{0})$ observed at point $P_{0}$ can be determined by adding up the contributions of all of these point sources after taking into account the geometric path lengths from all of these sources to $P_{0}$.  This concept is mathematically expressed in a Huygen-Fresnel integral:

\begin{equation}
\begin{split}
U(P_{0})&=\iint_{\Sigma}h(P_{0},P_{1})U(P_{1}) ds\\
\text{where  } h(P_{0},P_{1})&=
\frac{1}{j \lambda}
\frac{\exp(jkr_{01})}{r_{01}}
\cos(\overrightarrow{n},\overrightarrow{r}_{01})
\end{split}
\label{eq:huygen}
\end{equation}

The function $h(P_{0},P_{1})$ combines a Green's function $\exp(jkr_{01}) / r_{01}$ representing a spherically expanding wave with what is known as an \textbf{obliquity term,} $\cos(\overrightarrow{n},\overrightarrow{r}_{01})$ (the cosine of the angle between $\overrightarrow{n}$ and $\overrightarrow{r}_{01}$). This Huygen-Fresnel integral sums the contributions from an infinite number of sources across the aperture surface $\Sigma$.  One can also think of the propagation of waves from these sources over the distance between $U(x_{1},y_{1})$ and $U(x_{0},y_{0})$ as a kind of filter that has an impulse response $h(P_{0},P_{1})$.

Elaborating on Eq.~\ref{eq:huygen}:
\begin{equation}
\begin{split}
U(x_{0},y_{0})&=\iint_{\Sigma}h(x_{0},y_{0};x_{1},y_{1})U(x_{1},y_{1}) dx_{1} dy_{1}\\ 
U(x_{0},y_{0})&=\iint_{-\infty}^{\infty}h(x_{0},y_{0};x_{1},y_{1})U(x_{1},y_{1}) dx_{1} dy_{1}, \\
\text{ with } U(x_{1},y_{1})&=0 \text{ outside the aperture }\Sigma 
\end{split}
\label{eq:huygen2}
\end{equation}

We now introduce the first assumption, known as the \textbf{small angle approximation}:
\begin{equation}
\begin{split}
cos(\overrightarrow{n},\overrightarrow{r}_{01})&\approx 1\\
r_{01}&\approx z\\
h(x_{0},y_{0};x_{1},y_{1})&\approx \frac{1}{j\lambda z}\exp(jkr_{01})
\end{split}
\label{eq:small_angle}
\end{equation}

\subsection{The Fresnel approximation}
This is an intermediate step on the way to the Fraunhofer approximation.  Given the expression for path length,
\begin{equation}
\begin{split}
r_{01}&=\sqrt{z^2+(x_{1}-x_{0})^2+(y_{1}-y_{0})^2}\\
&=z\sqrt{1+\bigg{(}\frac{(x_{1}-x_{0})}{z}\bigg{)}^2
                  +\bigg{(}\frac{(y_{1}-y_{0})^2}{z}\bigg{)}}
\end{split}
\label{eq:r01_factor}
\end{equation}

we introduce a truncated binomial expansion:
\begin{equation}
\sqrt{1+b}=1 + \frac{1}{2}b - \frac{1}{8}b^2 + \ldots
\end{equation}
used to approximate $r_{01}$ truncated to 2 terms:
\begin{equation}
r_{01}\approx z \Bigg{[} 1+ \frac{1}{2}\bigg{(}\frac{(x_{1}-x_{0})}{z}\bigg{)}^2
                                              + \frac{1}{2}\bigg{(}\frac{(y_{1}-y_{0})}{z}\bigg{)}^2\Bigg{]}
\label{eq:binomial}
\end{equation}

Therefore:
\begin{equation}
h(x_{0},y_{0};x_{1},y_{1})\approx 
\frac{\exp(jkz)}{j\lambda z} 
\exp\Bigg{[} \frac{jk}{2z} \bigg{[}(x_{1}-x_{0})^2+(y_{1}-y_{0})^2\bigg{]}\Bigg{]}
\label{eq:h_approx}
\end{equation}

\subsection{The Fraunhofer approximation}
After rearranging terms:
\begin{equation}
\begin{split}
h(x_{0},y_{0};x_{1},y_{1})\approx
\frac{\exp(jkz)}{j\lambda z}
&\exp\Bigg{[} \frac{jk}{2z} \bigg{[}(x_{0}^2+y_{0}^2)\bigg{]}\Bigg{]}\\
&\times\exp\Bigg{[} \frac{jk}{2z} \bigg{[}(x_{1}^2+y_{1}^2)\bigg{]}\Bigg{]} 
             \exp\Bigg{[} -\frac{jk}{z} \bigg{[}(x_{0}x_{1}+y_{0}y_{1})\bigg{]}\Bigg{]}
\end{split}
\label{eq:h_approx2}
\end{equation}

The Fraunhofer assumption is that the distance between the aperture plane and the observation plane is much greater than the dimensions of the aperture function, i.e.:
\begin{equation}
z>>\frac{k(x_{1}^2+y_{1}^2)_{\text{max}}}{2}
\label{eq:fa_approx}
\end{equation}

The distance at which this approximation becomes reasonable is known as the \textbf{Fraunhofer distance}. Thus this quadratic phase term in Eq.~\ref{eq:h_approx2} is assumed to be $\approx 1$. Finally:
\begin{equation}
U(x_{0},y_{0}) \approx 
\frac {\exp(jkz)\exp\Big{[}\frac{jk}{2z}(x_{0}^2+y_{0}^2)\Big{]}} {j\lambda z} 
\iint_{-\infty}^{\infty} U(x_{1},y_{1}) \exp\bigg{[} -\frac{j2\pi}{\lambda z}(x_{0}x_{1}+y_{0}y_{1})\bigg{]}dx_{1} dy_{1}
\label{eq:fa_approx2}
\end{equation}

Aside from the quadratic phase term outside the integral, this result is the Fourier transform of $U(x_{1},y_{1})$ evaluated at spatial frequencies
\begin{equation}
%\begin{split}
f_{x}=\frac{x_{0}}{\lambda z} \text{~~~and~~~}
f_{y}=\frac{y_{0}}{\lambda z}.
%\end{split}
\label{eq:fa_approx3}
\end{equation}
%\textbf{TA-DAAAA!} (\textit{Trans:} QED.)
%\clearpage

\section{The two-dimensional Fourier transform}
The Fraunhofer approximation is a powerful tool for understanding ultrasound beamforming. Its intuitive application requires knowledge of 2-D Fourier transforms and transform properties. These are seen to be extensions of the Fourier transforms and properties in 1-D, with the addition of the issue of \textbf{separability}.

\subsection{The analytic form of the 2-D Fourier transform}
Following Bracewell's \textit{The Fourier Transform and Its Applications}\cite{Bracewell86}, the relationships between a 2-D function $f(x,y)$ and its 2-D Fourier transform are:

\begin{equation}
\begin{split}
F(u,v) &= \iint^{\infty}_{-\infty}f(x,y)~exp\big{(}-j 2\pi(ux+vy)\big{)}~dx~dy\\
f(x,y) &= \iint^{\infty}_{-\infty}F(u,v)~exp\big{(}j 2\pi(ux+vy)\big{)}~du~dv\\
\end{split}
%\label{}
\end{equation}

The convolution integral is another extension to 2-D of a familiar relationship:
\begin{equation}
%\begin{split}
f(x,y) \ast g(x,y) = \iint^{\infty}_{-\infty}f(\eta,\nu)~g(x-\eta,y-\nu)~d\eta~d\nu
%\end{split}
%\label{}
\end{equation}

A 2-D function is \textbf{separable} if it can be described as the product of two 1-D functions along orthogonal dimensions:
\begin{equation}
\begin{split}
f(x,y) &= f(x)f(y)\\
F(u,v) &= F(u)F(v)\\
\end{split}
%\label{}
\end{equation}

Thus if a function is separable in the space domain, it is also separable in the frequency domain, and vice versa.  Identifying a function as separable can simplify its analysis by reducing its 2-D transform to a product of 1-D transforms. Identifying a function as separable can simplify its analysis by reducing its 2-D transform to a product of 1-D transforms.
 
One class of functions that are \textbf{not} separable are those having \textbf{circular symmetry}. The 2-D Fourier transforms of these functions are also circularly symmetric:
\begin{equation}
\begin{split}
\text{If~~} f(x,y) &= f(r)\text{,~where~} r^2=x^2+y^2\\
\text{then~~} F(u,v) &= F(q)\text{,~where~} q^2=u^2+v^2\\
\end{split}
%\label{}
\end{equation}

A series of Fourier transform pairs are shown in Figures \ref{fig:comb_2dft} through \ref{fig:tri_2d} using Bracewell's notation for the delta ($\delta$), comb (III), rectangle ($\Pi$), and triangle ($\Lambda$) functions.

%\enlargethispage{1.0in}
\begin{figure}[h]
\centering
\includegraphicslarge{figures/comb_2dft.eps}
\caption{}
\label{fig:comb_2dft}
\end{figure}
\mbox{}

\begin{figure}[h!]
\centering
\includegraphicslarge{figures/line_2d.eps}
\caption{}
\label{fig:line_2d}
\end{figure}
\mbox{}

\begin{figure}[h!]
\centering
\includegraphicslarge{figures/delta_2d.eps}
\caption{}
\label{fig:delta_2d}
\end{figure}
\mbox{}

\begin{figure}[h!]
\centering
\includegraphicslarge{figures/cos_2d.eps}
\caption{}
\label{figcos_2d:}
\end{figure}
\mbox{}

\begin{figure}[h!]
\centering
\includegraphicslarge{figures/cosrot_2d.eps}
\caption{}
\label{fig:cosrot_2d}
\end{figure}
\mbox{}

\begin{figure}[h!]
\centering
\includegraphicslarge{figures/cosxcosy_2d.eps}
\caption{}
\label{fig:cosxcosy_2d}
\end{figure}
\mbox{}

\begin{figure}[h!]
\centering
\includegraphicslarge{figures/gauss_2d.eps}
\caption{}
\label{fig:gauss_2d}
\end{figure}
\mbox{}

\begin{figure}[h!]
\centering
\includegraphicslarge{figures/rect_2d.eps}
\caption{}
\label{fig:rect_2d}
\end{figure}
\mbox{}

\begin{figure}[h!]
\centering
\includegraphicslarge{figures/tri_2d.eps}
\caption{}
\label{fig:tri_2d}
\end{figure}
\mbox{}

\subsection{Important properties of the 2-D Fourier transform}
\begin{table}[h]
\centering
\begin{tabular}{r l l}
Scaling:&$~~f(ax,by)~~$&$\Longleftrightarrow~~\frac{1}{|ab|}F\big{(}\frac{u}{a},\frac{v}{b}\big{)}$\\
\\
Addition:&$~~f(x,y)+g(x,y)~~$&$\Longleftrightarrow~~F(u,v)+G(u,v)$\\
\\
Translation~/~shift:&$~~f(x-a,y-b)~~$&$\Longleftrightarrow~~exp\big{(}-2\pi j(au+bv)\big{)}F(u,v)$\\
\\
Convolution:&$~~f(x,y)\ast g(x,y)~~$&$\Longleftrightarrow~~F(u,v)~G(u,v)$\\
Autocorrelation:&$~~f(x,y)\ast f^\ast(-x,-y)~~$&$\Longleftrightarrow~~|F(u,v)^2|$\\
Cross-correlation:&$~~f(x,y)\ast g^\ast(-x,-y)~~$&$\Longleftrightarrow~~F(u,v)~G^{\ast}(u,v)$\\
\\
Power:& \multicolumn{2}{l}{$~~\int^{\infty}_{-\infty}f(x,y)g^{\ast}(x,y)~dx~dy~~=~~\int^{\infty}_{-\infty}F(u,v)~G^{\ast}(u,v)~du~dv$}
%\label{}
\end{tabular}
\end{table}

\subsection{Transforms and properties in the context of a typical aperture}

The basis set of the Fourier transform is the complex exponential of general form $A\exp(j2\pi f x + \theta)$. In systems with circular symmetry, such as most optical systems constructed with circular lenses, a different basis set and transform are usually applied, i.e.\ Bessel functions and the Hankel transform. These allow a circularly symmetric 2-D problem to be solved as a 1-D function of radius $r$. In ultrasound, functions with this symmetry are encountered in work with circular pistons.  In this text, we restrict our application of the Fraunhofer approximation to the Fourier transform.

The aperture of a typical 1-D linear array ultrasound transducer is rectangular with electronic focusing applied along the longer dimension (lateral) and an acoustic lens providing a fixed focus in the second dimension (elevation). With no \textbf{apodization} (amplitude weighting of the elements) this aperture function can be represented as a separable product of two rectangle functions, $\Pi(x)\Pi(y)$. As seen in Figure \ref{fig:rect_2d}, the far-field amplitude pattern of this aperture on transmit or receive is approximated by $\text{sinc}(u)\text{sinc}(v)$.  

When this aperture is used on \textit{both} transmit and receive, as is almost always the case, the corresponding function is $\text{sinc}^2(u)\text{sinc}^2(v)$, seen in Figure \ref{fig:tri_2d}. In both cases, the far-field pattern is approximated by these Fourier transforms evaluated at spatial frequencies $f_{u}= x_{o} / \lambda z$ and $f_{v}=y_{o}/ \lambda z$. Intuitively, the transmit beam pattern shows where sound is sent \textit{into} the field, while the receive beam pattern shows the spatial \textit{sensitivity} of the transducer to returning echoes. 

To achieve electronic focusing, one or more dimensions of an aperture is typically divided into discrete elements that are themselves small rectangles. The subdivided dimension is more accurately modeled as a small rectangle convolved with a comb function the length of the array. Thus an aperture of size $D$ composed of elements of size $d$ is modeled as $\Pi(D)~[\Pi(d)\ast\text{III}(x/d)]$, where $\text{III}(x)=\Sigma^{\infty}_{-\infty}\delta(x-n)$. The transform of this function is a narrow sinc function convolved with a comb, all multiplied by a broad sinc function:
\begin{equation}
\Pi(D)~[\Pi(d)\ast\text{III}(x/d)]~~\Longleftrightarrow~~
\text{sinc}(u/d)~[\text{sinc}(u/D)\ast \text{III}(d u)]
\label{eq:grating}
\end{equation}
This is shown schematically in Figure \ref{fig:grating}.

\begin{figure}[h]
\centering
\includegraphicsfull{figures/grating.eps}
\caption{(Left) An aperture of size $D$ composed of elements of size $d$ is modeled as $\Pi(D)~[\Pi(d)\ast\text{III}(x/d)]$.  The Fourier transform of this pattern (right) is $\text{sinc}(u/d)~[\text{sinc}(u/D)\ast\text{III}(d u)]$. The subdivision of the array into elements produces grating lobes.}
\label{fig:grating}
\end{figure}

The transforms of the individual element and of the entire array have a number of implications.  First, each individual element has its own far-field amplitude pattern. This pattern in part determines the angular sensitivity of the element, and by extension, of the entire array.  This shows up as the broad sinc pattern in Figure \ref{fig:grating}.  Also, the periodicity created by subdividing the array introduces a periodicity in the beam pattern.  The desired \textbf{main lobe} of the beam pattern is now duplicated off axis as a series of \textbf{grating lobes}.  These can be a source of image artifacts, as they literally correspond to regions of unintended off-axis emission and sensitivity.  The magnitude of these grating lobes is an important design parameter. When the element spacing, or \textbf{pitch}, is reduced to $\lambda$, the side lobes are located at $90^\circ$ off the main lobe. With a pitch of $\lambda/2$ , the grating lobes are practically eliminated. This approach is necessary when the beam is to be aggressively steered off axis, such as in a cardiac probe.  The array's angular sensitivity and the distance of the grating lobes off-axis are both dependent on the element size, so it is common to increase the element size beyond $\lambda/2$ and rely on the reduced angular sensitivity of the elements to reduce the impact of the grating lobes.  This design approach is appropriate for linear arrays that will not be used with aggressive beam steering.

\section{Propagation as a spatial transfer function}
Under most circumstances in medical imaging, propagation is a linear operation. It has a transfer function associated with it, which is in turn the Fourier transform of the impulse response. Harmonic imaging, based on non-linear wave propagation, is also commonly used in clinical scanners but is beyond the scope of this discussion.

The free-space Green's function is an impulse function.  We can use it to represent a point source, and therefore its Fourier transform is the transfer function of propagation, $H(f_x,F_y) = \exp(jkz)\exp(-j\pi \lambda z(f_{x}^2+f_{y}^2))$.   %Let's think about this source in terms of spatial frequencies in the aperture.
Note that only a change in phase occurs in this operation.  There is no change in amplitude, i.e.\ a pattern at the Fraunhofer distance is the Fourier transform of the aperture, and its Fourier transform gives the aperture distribution back again.   

Remember that for an unfocused transducer the FA is only valid beyond the Fraunhofer distance, $z>> k(x_1^2+y_1^2)_{max}$, such that the greatest off-axis point in the aperture function satisfies the small angle approximation. For a focused transducer, the FA is valid in the focal plane.  This is because at the focal point, the spherically converging waves emanating from the focused aperture are indistinguishable from plane waves modulated by a much more distant, unfocused aperture. Focusing brings the far-field pattern to the focal plane by equalizing the accumulated phase due to propagation across the aperture.

To summarize, \textit{k}-space analysis is based on representing an optical or ultrasound system as a linear system in complex amplitude. Propagation itself can be represented as a transfer function.  Under the conditions of linear propagation, only the phase and amplitude of frequency components are modified.

%Example of calculation of the Fraunhofer distance:
%If $f_x$ = 1 cycle/m spatial frequency and observation field
%z = 100 m
%$\lambda$ = 1.5 mm
%0.15 meter off axis equals the point source location.
%This can also be determined through the geometry.