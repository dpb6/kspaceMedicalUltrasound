\chapter{A beginner's guide to speckle} 
An inherent characteristic of coherent imaging, including ultrasound imaging, is the presence of speckle noise. Speckle is a random, deterministic, interference pattern in an image formed with coherent radiation of a medium containing many sub-resolution scatterers. The texture of the observed speckle pattern does not correspond directly to underlying structure.  The local brightness of the speckle pattern, however, does reflect the local echogenicity of the underlying scatterers.

Speckle has a negative impact on ultrasound imaging.  Bamber and Daft show a reduction of lesion detectability of approximately a factor of eight due to the presence of speckle in the image\cite{Bamber86}. This radical reduction in contrast resolution is responsible for the poorer effective resolution of ultrasound compared to x-ray and MRI.

Speckle is present in both RF data and envelope-detected data.  In Figure \ref{fig:psf_demo} we see a simple conceptual demonstration of the impact of speckle noise on information content.  The object of interest is a hypoechoic lesion of 5 mm diameter with -9 dB contrast. The echogenicity map corresponding to this object is shown in the top left panel of Figure \ref{fig:psf_demo}, opposite the corresponding scattering function in the top right panel.  The scattering function
%is merely a field of zero-mean, normally distributed random numbers
%that 
represents the population of sub-resolution scatterers being imaged, and that are weighted in amplitude by the echogenicity map.  This scattering function was convolved with the point spread function of a hypothetical 7.5 MHz array (60 \% bandwidth, imaging at f/1).  The resulting RF echo data is shown in the lower left panel.  The RF echo data is zero-mean and thus does not show what is really of interest, i.e.\ a map of local echogenicity, or local echo magnitude. Envelope detection removes the 7.5 MHz carrier, producing the desired image of echo magnitude in the lower right panel.  Here it is easy to see how speckle noise obscures the information in the image.  While the mean speckle brightness at each region of the image reflects the original echogenicity map, the speckle noise itself does not reflect the structure, or information, in either the echogenicity map or the corresponding scattering function.

\begin{figure}[htbp]
\centering
\includegraphicslarge{figures/psf_demo.eps}
\caption{The ultrasound image of a hypoechoic lesion of 5 mm diameter with -9 dB contrast is considered.  The echogenicity map corresponding to this object is shown in the top left panel, opposite the corresponding scattering function in the top right panel.  The scattering function 
%is merely a field of zero-mean, normally
%distributed random numbers that 
represents the population of sub-resolution scatterers being imaged, and that are weighted in amplitude by the echogenicity map.  This scattering function was convolved with a point spread function to produce the RF echo data is shown in the lower left panel.  The RF echo data is zero-mean and thus does not show what is really of interest, i.e.\ a map of local echogenicity, or local echo magnitude. Envelope detection removes the carrier, producing the desired image of echo magnitude in the lower right panel. The differences between this image and the original echogenicity map arise from speckle noise.}
\label{fig:psf_demo}
\end{figure}
\clearpage

\section{The statistics of fully-developed speckle}
Given the stochastic nature of speckle noise, we must describe this noise pattern statistically to draw general conclusions about imaging systems.  The statistics used here to describe ultrasound speckle are drawn from the literature of laser optics\cite{Goodman85}.  Each of the diffuse scatterers in the isochronous volume contributes a component to the echo signal in a sum known as a \textbf{random walk} in the complex plane. This is shown schematically in Figure \ref{fig:ray_walk}.  If each step in this walk is considered an independent random variable, over many such walks we can apply the Central Limit Theorem to their sum.  Therefore, in fully developed speckle, this complex radio-frequency echo signal from diffuse scatterers alone has a zero mean, two-dimensional Gaussian probability density function (PDF) in the complex plane.

\begin{figure}[h]
\centering
\includegraphicslarge{figures/ray_walk.eps}
\caption{\small (Left) Through superposition, each scatterer in a population of diffuse scatterers contributes an echo signal that adds one step in a \textbf{random walk} that constitutes the resulting received complex echo $\boldsymbol{r}$. (Right) A contour plot of the PDF of $\boldsymbol{r}$, a 2-D complex Gaussian centered at the origin. The values of the magnitude of $\boldsymbol{r}$ for many such scatterer populations follow the Rayleigh PDF.}
\label{fig:ray_walk}
\end{figure}

Envelope detection removes the phase component, creating a signal with a Rayleigh amplitude PDF:
\begin{equation}
p_{A}(a)=\frac{a}{\sigma^2}exp\bigg{(}-\frac{a^2}{2\sigma^2}\bigg{)}, a\geq0
\label{eq:rayleigh}
\end{equation}

Speckle brightness is greater if there are fewer, longer steps in the random walk than if there are many shorter steps.  This could be accomplished by improving the spatial resolution of the system. On the other hand, if the scatterer density is doubled, a $\sqrt{2}$ increase in brightness results.
%Phase aberration decreases in the speckle brightness by redistributing
%the bright scatterers out to where they contribute little.(????????)

\section{Coherent components and Rician statistics}
When a coherent component is introduced to the speckle, it adds a constant strong phasor to the diffuse scatterers echoes and shifts the mean of the complex echo signal away from the origin in the complex plane, as shown in Figure \ref{fig:ric_walk}.  Upon detection, this has the effect of changing the Rayleigh PDF into a Rician PDF.  The Rician PDF is defined by the following equation:
\begin{equation}
p_{A}(a)=
	\frac{a}{\sigma^2}
	exp\bigg{(}-\frac{a^2+s^2}{2\sigma^2}\bigg{)}
	I_{0}\frac{as}{\sigma^2}, a\geq0
\label{eq:rician}
\end{equation}

\begin{figure}[h]
\centering
\includegraphicslarge{figures/ric_walk.eps}
\caption{\small (Left) The presence of a coherent component in the scatterer population adds a constant vector $\boldsymbol{s}$ to the random walk. (Right) A contour plot of the PDF of $\boldsymbol{r}$, a 2-D complex Gaussian centered at the end of $\boldsymbol{s}$. The values of the magnitude of $\boldsymbol{r}$ for a particular $\boldsymbol{s}$ follow the Rician probability PDF over many different populations of diffuse scatterers.}
\label{fig:ric_walk}
\end{figure}

These PDFs are nonzero for $a \geq0$ only.  The parameter $s$ is the echo strength of the bright scatterer, while $\sigma$ is the standard deviation of the complex Gaussian described above, i.e.\ both the real part and the imaginary part have variances of $\sigma$.  $I_{0}$ is the incomplete Bessel function of zero order.  The Rician PDF is parameterized by the variable $k$, which is defined as $s/\sigma$\cite{Goodman85}. The Rician PDF reduces to the Rayleigh PDF for the special case $s = 0$.  A family of Rician PDFs for various values of $k$, including the Rayleigh PDF, is shown in Figure \ref{fig:fam_rician}.

\begin{figure}[h]
\centering
\includegraphicsmed{figures/fam_rician.eps}
\caption{\small The Rayleigh PDF ($k=0$) and a family of Rician PDFs, parameterized by the variable $k$.}
\label{fig:fam_rician}
\end{figure}

%Two statistics commonly used to characterize the detected echo signal are the mean brightness $\mu$, and the standard deviation of brightness, which will be referred to with the variable $\sigma_{a}$ in this text to avoid confusion with the original complex RF signal standard deviation $\sigma$.
